Adobe Jenson
Albertus
Aldus
Alexandria
Algerian
American Typewriter
Antiqua
Arno*
Aster
Aurora
News 706
Baskerville
Bell
Belwe Roman
Bembo
Berkeley Old Style
Bernhard Modern
Bodoni
Bauer Bodoni
Bookman
Bulmer
Caledonia
Californian FB
Calisto MT
Cambria
Capitals
Cartier
Caslon
Wyld
Caslon Antique/Fifteenth Century
Catull
Centaur
Century Old Style*
Century Schoolbook*
New Century Schoolbook*
Century Schoolbook Infant*
Charis SIL
Charter
Cheltenham
Clearface
Cochin
Colonna
Computer Modern
Concrete Roman
Constantia
Cooper Black
Copperplate Gothic
Corona
News 705
DejaVu Serif
Didot
Droid Serif
Elephant
Emerson
Excelsior
News 702
Fairfield
FF Scala
Footlight
FreeSerif
Friz Quadrata
Garamond
Gentium
Georgia
Gloucester
Goudy Old Style/Goudy
Granjon
High Tower Text
Hoefler Text
IBM Plex Serif*
Imprint
Ionic No.5
News 701
ITC Benguiat
Janson
Jokerman
Joanna
Korinna
Lexicon
Liberation Serif
Linux Libertine
Literaturnaya
Lucida Bright*
Melior
Memphis
Miller
Minion*
Modern
Mona Lisa
Mrs Eaves*
MS Serif
New York
Nimbus Roman
NPS Rawlinson Roadway
OCR A Extended
Palatino
Book Antiqua
Perpetua
Plantin
Playbill
Primer
PT Serif
Renault
Requiem
Rotis Serif*
Sabon
Sistina
Source Serif Pro
Souvenir
STIX
Sylfaen
Theano Didot
Times New Roman
Times
Torino
Trajan
Trinité
Trump Mediaeval
Utopia
Vera Serif
Wide Latin
Windsor
XITS
