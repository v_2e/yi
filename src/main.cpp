#include <QApplication>

//#include "mainwindow.h"
#include "sandbox.h"

#include <QFontDatabase>
#include <PoolTest.h>
#include <QDebug>
#include <QWidget>
#include <QPushButton>

#include <iostream>
#include <iomanip>




typedef QString string;
class MathVariant{

public:
    enum Id{
        Normal = 0,
        Bold = 1,
        Italic = 2,
        BoldItalic = 3,
        DoubleStruck = 4,
        BoldFraktur = 5,
        Script = 6,
        BoldScript = 7,
        Fraktur = 8,
        Monospace = 9
    };

    static string texCommand(MathVariant::Id variant){
        return tex[variant];
    }

private:
    static string tex[];
    static string mml[];

};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Sandbox s;
    s.show();
    return a.exec();
}



