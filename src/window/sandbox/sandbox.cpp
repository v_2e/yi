#include "sandbox.h"
#include "ui_sandbox.h"

#include <iostream>
#include <vector>

#include <map>

#include "ComponentFactory.h"

#include <array>
#include <variant>
#include <QDebug>
#include <QElapsedTimer>
#include <QTimer>

#include "Symbol.h"
#include "Row.h"
#include "Container.h"
#include "Fraction.h"

#include "MathWidget.h"
#include "Document.h"
#include <QSpinBox>

#include "MMLStream.h"
#include <QFile>

#include "ComponentFactory.h"
#include "GlyphProvider.h"

#include <QKeyEvent>
#include "MathMLPresenter.h"
#include "UnicodeHelper.h"

void render_component(Component* component){
    static int id = 0;
    qDebug() << "Size: " << component->size();
    QPixmap pixmap(component->size());
    pixmap.fill(Qt::white);
    component->render(&pixmap);
    pixmap.save(QString("/home/oleksiy/yi_components/component_%1.png").arg(id++));
}

Sandbox::Sandbox(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Sandbox)
{
    ui->setupUi(this);



    IGlyphProvider* drawableProvider = new GlyphProvider();
    ComponentFactory* components = new ComponentFactory(drawableProvider);

    doc = new Document();
    DocumentEditor* editor = &doc->editor();
    const Cursor* cursor = &editor->cursor();

    connect(ui->addButton, &QPushButton::clicked, this, [this, components](){
        Container *n = new Container({{components->createIdentifier('a')}});
        Row *d = new Row({components->createIdentifier('b')});
        Fraction *f = new Fraction({n, d});

        const Cursor& c = doc->editor().cursor();
        doc->editor().insert(c.focus(), c.pos(), f);
    });

    connect(ui->addContainerButton, &QPushButton::clicked, this, [this, components](){
        Container *n = new Container({{components->createIdentifier('p')}});
        Row *d = new Row({components->createIdentifier('A'),
                          components->createIdentifier('+'),
                          components->createIdentifier('q')});
        Fraction *f = new Fraction({n, d});
        const Cursor& c = doc->editor().cursor();
        doc->editor().insert(c.focus(), c.pos(), f);

        Row* focus = qrand()% 2 == 0 ? d : n->frontRow();
        doc->editor().setCursor(focus, 0);
    });

    connect(ui->addMultipleButton, &QPushButton::clicked, this, [this, components](){

        std::vector<std::vector<Component*>> v = {{components->createIdentifier('1'),
                                                   components->createIdentifier('2'),
                                                   components->createIdentifier('3'),},
                                                  {components->createIdentifier('4'),
                                                   components->createIdentifier('5')},
                                                  {components->createIdentifier('6'),
                                                   components->createIdentifier('7'),
                                                   components->createIdentifier('8'),
                                                   components->createIdentifier('9')},
                                                  {},
                                                  {},
                                                  {components->createIdentifier('2'),
                                                   components->createIdentifier('4'),
                                                   components->createIdentifier('1'),
                                                   components->createIdentifier('4')}};

        DocumentEditor& editor = doc->editor();
        Cursor const& cursor = editor.cursor();

        doc->editor().insert(cursor.focus(), cursor.pos(), std::move(v));


    });

    connect(ui->attachDetachButton, &QPushButton::clicked, this, [this](){
        static bool attached = false;
        attached = !attached;
        if(attached){
            ui->math->setDocument(doc);
            ui->attachDetachButton->setText("Detach");
        }else{
            ui->math->setDocument(nullptr);
            ui->attachDetachButton->setText("Attach");
        }
    });



    connect(ui->math, &MathWidget::keyPressed, this, [this, components, editor, cursor](QKeyEvent* ke){

        QElapsedTimer timer;
        timer.start();

        if(ke->matches(QKeySequence::Undo)){
            doc->editor().undo();
        }else if(ke->matches(QKeySequence::Redo)){
            doc->editor().redo();
        }else if(ke->matches(QKeySequence::Save)){
            QElapsedTimer timer;
            timer.start();
            QFile file2("/home/oleksiy/test.mml");
            if(file2.open(QFile::WriteOnly)){
                MathMLPresenter mmlp(file2);
                mmlp.writeMathML(&doc->root());
                file2.flush();
                file2.close();
                qDebug() << "Saved in " << timer.elapsed() << " ms.";
            }

        }else if(ke->matches(QKeySequence::Find)){
            Container *n = new Container({{components->createIdentifier('p')}});
            Row *d = new Row({components->createIdentifier('A'),
                              components->createIdentifier('+'),
                              components->createIdentifier('q')});
            Fraction *f = new Fraction({n, d});
            const Cursor& c = doc->editor().cursor();
            doc->editor().insert(c.focus(), c.pos(), f);

            Row* focus = qrand()% 2 == 0 ? d : n->frontRow();
            doc->editor().setCursor(focus, 0);
        }else{
            if(ke->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier)){
                switch (ke->key()) {
                case Qt::Key_D:
                    editor->insert(cursor->focus(), cursor->pos(), this->doc->root().clone());
                    break;
                case Qt::Key_W:{
                    Component* s = components->createNode(
                                     Component::Type::Sup,
                                     {
                                      new Row({components->createIdentifier('a')}),
                                      new Row({components->createIdentifier('b')})
                                     }
                                );
                    editor->insert(cursor->focus(), cursor->pos(), s);
                 }break;
                }
            }else{
                switch (ke->key()) {
                case Qt::Key_Backspace:
                    if(cursor->pos() > 0){
                        editor->remove(cursor->focus(), cursor->pos() - 1);
                    }
                    break;
                case Qt::Key_Delete:
                    if(cursor->pos() < cursor->focus()->childrenCount()){
                        editor->remove(cursor->focus(), cursor->pos());
                    }
                    break;
                default:
                    if (Qt::Key_nobreakspace > ke->key() && ke->key() < Qt::Key_ydiaeresis){
                        bool capital = ke->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier);

                        QString s(ke->key());
                        char32_t c = capital ? s.toUpper().toUcs4().first() :
                                               s.toLower().toUcs4().first();

                        Component* comp = components->createIdentifier(c);
                        doc->editor().insert(doc->editor().cursor().focus(),
                                             doc->editor().cursor().pos(),
                                             comp);
                    }
                }
            }
        }
        qDebug() << "Key event processed in " << timer.elapsed() << " ms.";

    });



    UnicodeHelper unicode;
    UnicodeHelper::Block blocks[] {
        UnicodeHelper::Block::LatinBold,
        UnicodeHelper::Block::LatinItalic,
        UnicodeHelper::Block::LatinItalicBold,
        UnicodeHelper::Block::LatinDoubleStruck,
        UnicodeHelper::Block::LatinFraktur
    };
    for(auto block : blocks){
        std::pair<char32_t, char32_t> range = unicode.range(block);
        char32_t l = range.first;
        const char32_t r = range.second;
        while(l <= r){
            Component* component = components->createIdentifier(l++);
            editor->insert(cursor->focus(), cursor->pos(), component);
        }
        editor->insert(cursor->focus(), cursor->pos(), {{},{}});
    }




    std::vector<std::pair<uint, uint>> ranges = {
        {'A', 'Z'},
        {'a', 'z'},
        {'0','9'},
        {0x0391, 0x03A9},  // greek uppercase
        {0x03B1, 0x03C9}  // greek lowercase
    };

    // Add math table
    for(uint i = 0x0; i <= 0xF; i++){
        uint group = 0x2200 + ((i << 4) & 0xF0);
        qDebug() <<  QString::number(group, 16 );
        ranges.push_back({group, group + 0xF});
    }

    for(uint i = 0x0; i <= 0xF; i++){
        uint group = 0x2A00 + ((i << 4) & 0xF0);
        qDebug() <<  QString::number(group, 16 );
        ranges.push_back({group, group + 0xF});
    }

    for(uint i = 0x40; i <= 0x7F; i++){
        uint group = 0x1D000 + ((i << 4) & 0xFF0);
        qDebug() <<  QString::number(group, 16 );
        ranges.push_back({group, group + 0xF});
    }


    for(auto range : ranges){
        char32_t l = range.first;
        const char32_t r = range.second;
        while(l <= r){
            Component* component = components->createIdentifier(l++);
//            render_component(component);
            editor->insert(cursor->focus(), cursor->pos(), component);
        }
        editor->insert(cursor->focus(), cursor->pos(), {{},{}});
    }

    ui->attachDetachButton->click();
    editor->setCursor(cursor->focus(), cursor->pos());
}

Sandbox::~Sandbox()
{
    delete ui;
}

void tests(){

    //    for(auto c : str){
    //        Component* component = components->createIdentifier(c);
    //        editor.insert(editor.cursor().focus(), editor.cursor().pos(), component);
    //    }


    //    for(int i = 0; i < 1; i++)
    //        ui->addButton->click();
    //    for(int i = 0; i < 2; i++)
    //        ui->addContainerButton->click();
    //    for(int i = 0; i < 3; i++)
    //        ui->cloneButton->click();


    //    DocumentEditor &editor = doc->editor();
    //    Component* row = editor.cursor().focus()->clone();
    //    for(uint i = 0; i < 50; i++){
    //        editor.insert(doc->root().backRow(),
    //                      doc->root().backRow()->childrenCount(),
    //                      {{},{row->clone()}});
    //    }
}
