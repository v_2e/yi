#ifndef SANDBOX_H
#define SANDBOX_H

#include <QMainWindow>
#include <QPainter>

#include <QWidget>

class Document;
class Row;
class Container;


namespace Ui {
  class Sandbox;
}

class Sandbox : public QMainWindow
{
    Q_OBJECT

public:
    explicit Sandbox(QWidget *parent = nullptr);
    ~Sandbox();

private:
    Ui::Sandbox *ui;
    Document *doc;

};

#endif // SANDBOX_H
