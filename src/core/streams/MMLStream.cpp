#include "MMLStream.h"

#include <QFile>
#include <QTextStream>


MMLStream::MMLStream(QTextStream& stream):
    stream(stream)
{
}

MMLStream& MMLStream::dive()
{
    offset += tagOffset;
    return *this;
}

MMLStream& MMLStream::lift()
{
    offset -= tagOffset;
    if(offset < 0)
        offset = 0;
    return *this;
}

MMLStream &MMLStream::write(const char32_t &c)
{
    if(writeOffset){
        make_offset();
        writeOffset = false;
    }
    stream << QString::fromUcs4(&c, 1);
    return *this;
}

MMLStream &MMLStream::write(const char *str)
{
    if(writeOffset){
        make_offset();
        writeOffset = false;
    }
    stream << str;
    return *this;
}

MMLStream& MMLStream::endline()
{
    endl(stream);
    writeOffset = true;
    return *this;
}

void MMLStream::make_offset()
{
    stream.setFieldWidth(offset);
    stream << ' ';
    stream.setFieldWidth(0);
}

