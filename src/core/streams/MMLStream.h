#ifndef MMLSTREAM_H
#define MMLSTREAM_H

#include <QTextStream>

typedef std::string string_t;

class MMLStream
{

public:
    MMLStream(QTextStream& stream);
    MMLStream& dive();
    MMLStream& lift();
    MMLStream& write(const char32_t& c);
    MMLStream& write(const char* str);
    MMLStream& endline();


private:
    QTextStream& stream;
    int tagOffset = 2;
    int offset = 0;
    bool writeOffset = false;
    void make_offset();

};


#endif // MMLSTREAM_H
