#ifndef TEXSTREAM_H
#define TEXSTREAM_H

#include <string>
#include <iostream>

class TeXStream
{
public:
    TeXStream(std::ostream& stream);

private:
    std::ostream& ostream;

};

#endif // TEXSTREAM_H
