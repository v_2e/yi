#ifndef MATHMLPRESENTER_H
#define MATHMLPRESENTER_H

#include "ComponentVisitor.h"
#include <QFile>
#include <MMLStream.h>

class Component;

class MathMLPresenter : public ComponentVisitor
{
public:
    MathMLPresenter(QFile &file);
    void writeMathML(Component* component);
    void visitRow(Row *row);
    void visitContainer(Container *container);
    void visitSymbol(Symbol *symbol);
    void visitFraction(Fraction *fraction);
    void visitSup(Sup *sup);

private:
    QTextStream textStream;
    MMLStream mmlStream;

};

#endif // MATHMLPRESENTER_H
