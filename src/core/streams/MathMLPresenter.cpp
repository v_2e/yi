#include "MathMLPresenter.h"

#include "Row.h"
#include "Container.h"
#include "Symbol.h"
#include "Fraction.h"
#include "Sup.h"


#include <QDebug>

MathMLPresenter::MathMLPresenter(QFile &file) :
    textStream(&file),
    mmlStream(textStream)
{
    assert(file.isOpen());
}

void MathMLPresenter::writeMathML(Component *component)
{
    component->accept(*this);
}

void MathMLPresenter::visitRow(Row *row)
{
    mmlStream.write("<mrow>").dive().endline();
    const uint n = row->componentsCount();
    for(uint i = 0; i < n; i++){
        Component* component = row->getComponent(i);
        component->accept(*this);
        mmlStream.endline();
    }
    mmlStream.lift().write("</mrow>");
}

void MathMLPresenter::visitContainer(Container *container)
{
    if(container->rowsCount() == 1){
       container->getComponent(0)->accept(*this);
    }else{
        mmlStream.write("<mtab>").endline().dive();
        const uint n = container->componentsCount();
        for(uint i = 0; i < n; i++){
            mmlStream.write("<tr>").endline().dive();
            Component* component = container->getComponent(i);
            component->accept(*this);
            mmlStream.endline().lift().write("</tr>").endline();
        }
        mmlStream.lift().write("</mtab>");
    }
}

void MathMLPresenter::visitSymbol(Symbol *symbol)
{
    if(symbol->getType() == Component::Type::Symbol){
        mmlStream.write("<mi>").write(symbol->getData()).write("</mi>");
    }else{
        mmlStream.write("<mo>").write(symbol->getData()).write("</mo>");
    }
}

void MathMLPresenter::visitFraction(Fraction *fraction)
{
    mmlStream.write("<mfrac>").endline().dive();
    fraction->getComponent(0)->accept(*this);
    mmlStream.endline();
    fraction->getComponent(1)->accept(*this);
    mmlStream.endline();
    mmlStream.lift().write("</mfrac>");
}

void MathMLPresenter::visitSup(Sup *sup)
{
    mmlStream.write("<msup>").endline().dive();
    sup->getComponent(0)->accept(*this);
    mmlStream.endline();
    sup->getComponent(1)->accept(*this);
    mmlStream.endline();
    mmlStream.lift().write("</msup>");
}
