#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <vector>

#include "Component.h"

class Clipboard
{

public:
    Clipboard();
    ~Clipboard();
    void put(const std::vector<std::vector<Component*>> &components);
    const std::vector<std::vector<Component*>>& get();

private:
    std::vector<std::vector<Component*>> buffer;
    void clear();

};

#endif // CLIPBOARD_H
