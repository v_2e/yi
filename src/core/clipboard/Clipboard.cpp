#include "Clipboard.h"

Clipboard::Clipboard()
{

}

Clipboard::~Clipboard()
{
    clear();
}

void Clipboard::put(const std::vector<std::vector<Component*>> &components)
{
    if(components.size() > 0)
        clear();
    buffer = components;
}

const std::vector<std::vector<Component*>>& Clipboard::get()
{
    return buffer;
}

void Clipboard::clear()
{
    for(auto line : buffer)
        for(auto c : line)
            delete c;
}
