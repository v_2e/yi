#ifndef UNICODEHELPER_H
#define UNICODEHELPER_H

#include <unordered_map>

class UnicodeHelper
{

public:
    enum Block{
        LatinBold = 0,
        LatinItalic = 1,
        LatinItalicBold = 2,
        LatinDoubleStruck = 3,
        LatinFraktur = 4
    };

public:
    UnicodeHelper();
    std::pair<char32_t, char32_t> range(Block block);

private:
     std::unordered_map<uint, std::pair<char32_t, char32_t>> blocks;

};

#endif // UNICODEHELPER_H
