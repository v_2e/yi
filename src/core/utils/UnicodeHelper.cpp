#include "UnicodeHelper.h"

UnicodeHelper::UnicodeHelper()
{
    blocks[Block::LatinBold] = {0x1D400, 0x1D433};
    blocks[Block::LatinItalic] = {0x1D434, 0x1D467};
    blocks[Block::LatinItalicBold] = {0x1D468, 0x1D49B};
    blocks[Block::LatinDoubleStruck] = {0x1D538, 0x1D56B};
    blocks[Block::LatinFraktur] = {0x1D56C, 0x1D59F};
}

std::pair<char32_t, char32_t> UnicodeHelper::range(UnicodeHelper::Block block)
{
    return blocks.at(block);
}
