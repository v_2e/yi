#include <QPainter>
#include <QPaintEvent>
#include <QDebug>
#include <QElapsedTimer>

#include "Symbol.h"
#include "ComponentVisitor.h"
#include "GlyphDrawable.h"

Symbol::Symbol(Component::Type type, IGlyphProvider *glyphDrawableProvider, char32_t c):
    Component (type),
    mDrawableProvider(glyphDrawableProvider),
    mSymbolCode(c)
{
    assert(type == Component::Type::Symbol || type == Component::Type::Operator);
    setAttribute(Qt::WA_StaticContents);

    mDrawableHandle = mDrawableProvider->provideDrawable({c});
    mGlyphDrawable = mDrawableHandle->drawable;

    updateGeometry();
}

Symbol::~Symbol()
{
    mDrawableProvider->recycleDrawable(mDrawableHandle);
}

Component *Symbol::clone()
{
    return new Symbol(getType(), mDrawableProvider, mSymbolCode);
}

char32_t Symbol::getData()
{
    return mSymbolCode;
}

void Symbol::accept(ComponentVisitor &visitor)
{
    visitor.visitSymbol(this);
}

void Symbol::paintEvent(QPaintEvent *pe)
{
    QPainter p(this);
//    drawBoundingRect(p, Qt::red);
//    drawBaseLine(p, Qt::red);

    p.setRenderHint(QPainter::Antialiasing);
    p.setRenderHint(QPainter::TextAntialiasing);

    p.setPen(Qt::black);
    double scale = 0.01 * getScale();
    p.scale(scale, scale);
    mGlyphDrawable->draw(mGlyphDrawPoint, p);

    pe->accept();
}

void Symbol::measure(Geometry &geometry)
{
    float scale = 0.01f * getScale();
    geometry.ascent = qRound(scale*mGlyphDrawable->ascent());
    geometry.descent =  qRound(scale*mGlyphDrawable->descent());
    geometry.leftBearing =  qRound(scale*mGlyphDrawable->leftBearing());
    geometry.horizontalAdvance =  qRound(scale*mGlyphDrawable->horizontalAdvance());
    geometry.width =  qRound(scale*mGlyphDrawable->width());
    geometry.height =  geometry.ascent + geometry.descent;

    mGlyphDrawPoint.setX(-mGlyphDrawable->leftBearing());
    mGlyphDrawPoint.setY(mGlyphDrawable->ascent());
}
