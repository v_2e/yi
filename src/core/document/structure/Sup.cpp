#include "Sup.h"
#include "ComponentVisitor.h"

Sup::Sup(std::vector<Component*> &&children):
    Node(Component::Type::Sup, std::move(children))
{
    assert(mChildren.size() == 2);
    mChildren.back()->setBaseScale(60);
    updateGeometry(false);
}

Component* Sup::clone()
{
    return new Sup(cloneChildren());
}

void Sup::accept(ComponentVisitor &visitor)
{
    visitor.visitSup(this);
}


void Sup::measure(Geometry &geometry)
{
    Component* base = mChildren.at(0);
    Component* sup = mChildren.at(1);

    int width = base->width() + sup->width();
    int height = base->height() + sup->height() / 2;
    base->move(0, sup->height() / 2);
    sup->move(base->width(), 0);


    geometry.width = width;
    geometry.height = height;
    geometry.ascent = base->ascent() + sup->height() / 2;
    geometry.descent = base->descent();
    geometry.leftBearing = 0;
    geometry.horizontalAdvance = width;
}

