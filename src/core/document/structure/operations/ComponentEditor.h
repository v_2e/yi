#ifndef COMPONENTEDITOR_H
#define COMPONENTEDITOR_H

#include "Component.h"

class Node;
class Row;
class Container;


class ComponentEditor
{

    friend class DocumentEditor;

private:
    explicit ComponentEditor();

public:
    // Row operations
    void insert(Row* row, const uint& at, Component* component,
                   bool notifyParent = true);
    void insert(Row* row, const uint& at, std::vector<Component*> &&components,
                   bool notifyParent = true);

    Component* remove(Row* row, const uint& at, bool notifyParent = true);
    std::vector<Component*> remove(Row* row,
                                   const uint& begin, const uint& end,
                                   bool notifyParent = true);


    // Container operations
    void insertRow(Container* container,
                   const uint& at,
                   Row* row,
                   bool notifyParent = true);
    void insertRows(Container* container,
                                 const uint& at,
                                 std::vector<Row*> &&lines,
                                 bool notifyParent = true);

    std::vector<Row*> removeRows(Container* container,
                                 const uint& begin_line, const uint& end_line,
                                 bool notifyParent = true);

    void replace(Node* node, Component* child, Component* with,
                 bool updateGeometry = true,
                 bool notifyParent = true);

    void inflate(Row* row, Container* container, bool notifyParent);
    Row* flatten(Container* container, bool notifyParent);

private:
    // Utils
    void swap(Node* parent, Component* child, Component* by);

};

#endif // COMPONENTEDITOR_H
