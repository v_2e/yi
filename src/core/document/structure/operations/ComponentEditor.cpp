#include "ComponentEditor.h"

#include "Node.h"
#include "Row.h"
#include "Container.h"


ComponentEditor::ComponentEditor()
{

}

// Row operations
void ComponentEditor::insert(Row *row,
                             const uint &at,
                             Component *component,
                             bool notifyParent)
{
    assert(row != nullptr);
    assert(component != nullptr);

    std::vector<Component*> &children = row->mChildren;

    assert(at <= children.size());

    children.insert(children.begin() + at, component);
    row->attach(component);

    row->updateGeometry(notifyParent);
}

void ComponentEditor::insert(Row *row,
                             const uint &at,
                             std::vector<Component *> &&components,
                             bool notifyParent)
{

    assert(row != nullptr);

    if(components.size() == 0)
        return;

    std::vector<Component*> &children = row->mChildren;
    assert(at <= children.size());

    children.insert(children.begin() + at, components.begin(), components.end());
    for(auto component : components)
        row->attach(component);

    row->updateGeometry(notifyParent);
}

Component* ComponentEditor::remove(Row *row,
                                   const uint &at,
                                   bool notifyParent)
{
    assert(row != nullptr);

    std::vector<Component*> &children = row->mChildren;
    assert(at < children.size());

    Component* component = children.at(at);
    children.erase(children.begin() + at);
    row->detach(component);

    row->updateGeometry(notifyParent);

    return component;
}

std::vector<Component*> ComponentEditor::remove(Row *row,
                                                const uint &begin,
                                                const uint &end,
                                                bool notifyParent)
{
    assert(row != nullptr);

    std::vector<Component*> &children = row->mChildren;
    assert(end <= children.size() && end >= begin);

    std::vector<Component*> v (children.begin() + begin, children.begin() + end);
    children.erase(children.begin() + begin, children.begin() + end);

    for(auto child : v)
        row->detach(child);

    row->updateGeometry(notifyParent);

    return v;
}

void ComponentEditor::insertRow(Container *container,
                                const uint &at,
                                Row *row,
                                bool notifyParent)
{
    assert(container != nullptr);
    assert(row != nullptr);

    std::vector<Component*> &children = container->mChildren;
    children.insert(children.begin() + at, row);
    container->attach(row);

    container->updateGeometry(notifyParent);
}


// Container operations
void ComponentEditor::insertRows(Container *container,
                                 const uint &at,
                                 std::vector<Row*> &&rows,
                                 bool notifyParent)
{
    assert(container != nullptr);

    if(rows.size() == 0)
        return;

    std::vector<Component*> &children = container->mChildren;
    children.insert(children.begin() + at, rows.begin(), rows.end());

    for(auto row : rows)
        container->attach(row);

    container->updateGeometry(notifyParent);
}

std::vector<Row*> ComponentEditor::removeRows(Container *container,
                                              const uint &begin_line,
                                              const uint &end_line,
                                              bool notifyParent)
{
    assert(container != nullptr);
    assert(begin_line < container->rowsCount());
    assert(end_line <= container->rowsCount() && begin_line <= end_line);


    std::vector<Component*> &rows = container->mChildren;

    const uint n = end_line - begin_line;
    std::vector<Row*> removed(n);

    for(uint i = 0; i < n; i++){
        Row* row = static_cast<Row*>(rows.at(begin_line + i));
        removed[i] = row;
        container->detach(row);
    }

    rows.erase(rows.begin() + begin_line, rows.begin() + end_line);

    container->updateGeometry(notifyParent);

    return removed;
}

void ComponentEditor::replace(Node *node, Component *child, Component *with,
                              bool updateGeometry,
                              bool notifyParent)
{
    int pos = node->find(child);
    assert(pos >= 0);

    node->detach(child);
    node->mChildren[static_cast<size_t>(pos)] = with;
    node->attach(with);

    if(updateGeometry)
        node->updateGeometry(notifyParent);
}

void ComponentEditor::inflate(Row *row, Container *container, bool notifyParent)
{
    Node* parent = row->getParent();

    assert(parent != nullptr);

    int pos = parent->find(row);
    assert(pos >= 0);
    parent->detach(row);
    insertRow(container, 0, row);
    parent->mChildren[static_cast<size_t>(pos)] = container;
    parent->attach(container);
    parent->updateGeometry(notifyParent);
}

Row *ComponentEditor::flatten(Container *container, bool notifyParent)
{
    Node* parent = container->getParent();

    assert(parent != nullptr);

    int pos = parent->find(container);
    assert(pos >= 0);

    parent->detach(container);
    Row* row = removeRows(container, 0, 1, false).front();

    parent->mChildren[static_cast<size_t>(pos)] = row;
    parent->attach(row);

    parent->updateGeometry(notifyParent);

    return row;
}



//Container* ComponentEditor::rowInsert(Row *row, const uint &at,
//                                      std::vector<std::vector<Component*>> &&components,
//                                      bool notifyParent)
//{
//    if(components.size() == 1){
//        rowInsert(row, at, std::move(components.at(0)), notifyParent);
//        return nullptr;
//    }else{
//        Node* parent = row->getParent();
//        if(parent != nullptr){
//            if(parent->getType() == Component::Type::Container){
//                uint line = 0;
//                for(auto c : parent->mChildren){
//                    if(c == row) break;
//                    line++;
//                }
//                insert(static_cast<Container*>(parent), line, at, std::move(components), notifyParent);
//                return nullptr;
//            }else{
//                if(row->isExpandable()){
//                    parent->detach(row);
//                    Container* expanded = expand(row);
//                    insert(expanded, 0, at, std::move(components));
//                    swap(parent, row, expanded);
//                    return expanded;
//                }else{
//                    size_t n = 0;
//                    for(auto l : components)
//                        n += l.size();
//                    std::vector<Component*> flatten(n);
//                    uint i = 0;
//                    for(auto l : components)
//                        for(auto c : l)
//                            flatten[i++] = c;
//                    components.clear();
//                    rowInsert(row, at, std::move(flatten));
//                    return nullptr;
//                }
//            }
//        }
//    }
//}



//void ComponentEditor::insert(Container *container,
//                             const uint &line, const uint &pos,
//                             std::vector<std::vector<Component*>> &&components,
//                             bool notifyParent)
//{
//    assert(container != nullptr);

//    std::vector<Component*> &children = container->mChildren;

//    assert(line < children.size());

//    Row* row = static_cast<Row*>(children.at(line));
//    assert(pos <= row->childrenCount());

//    const uint n_lines = static_cast<uint>(components.size());
//    if(n_lines > 1){
//        std::vector<Component*> rest = rowRemove(row, pos, row->childrenCount(), false);

//        if(rest.size() != 0){
//            std::vector<Component*> &last_row = components.back();
//            last_row.insert(last_row.end(), rest.begin(), rest.end());
//        }

//        std::vector<Component*> rows(n_lines - 1);
//        for(uint i = 0; i < n_lines - 1; i++){
//            rows[i] = new Row(std::move(components.at(i + 1)));
//            container->attach(rows[i]);
//        }
//        children.insert(children.begin() + line + 1, rows.begin(), rows.end());

//        rowInsert(row, pos, std::move(components.front()), false);

//        container->updateGeometry(notifyParent);
//    }else{
//        if(components.front().size() > 0)
//            rowInsert(row, pos, std::move(components.at(0)), notifyParent);
//    }
//}


//std::vector<std::vector<Component*>>
//ComponentEditor::remove(Container *container,
//                        const uint &begin_line, const uint &begin_pos,
//                        const uint &end_line, const uint &end_pos,
//                        bool notifyParent)
//{
//    qDebug() << "ComponentEditor: remove.";
//    assert(container != nullptr);

//    std::vector<Component*> &lines = container->mChildren;

//    assert(begin_line < lines.size());
//    assert(end_line < lines.size() && begin_line <= end_line);

//    const uint n_lines = end_line - begin_line + 1;

//    std::vector<Row*> rows(n_lines - 1);
//    for(uint i = 1; i < n_lines; i++){
//        Row *row = static_cast<Row*>(lines[begin_line + i]);
//        container->detach(row);
//        rows[i - 1] = row;
//    }
//    lines.erase(lines.begin() + begin_line + 1,
//                lines.begin() + end_line + 1);

//    std::vector<std::vector<Component*>> removed(n_lines);

//    Row* baseRow = static_cast<Row*>(lines.at(begin_line));
//    removed[0] = rowRemove(baseRow, begin_pos, baseRow->childrenCount(), false);
//    for(uint i = 1; i < n_lines - 1; i++)
//        removed[i] = rowRemove(rows[i - 1], 0, rows[i - 1]->childrenCount());
//    removed[n_lines - 1] = rowRemove(rows[n_lines - 2], 0, end_pos, false);

//    Row* lastRow = rows[n_lines - 2];
//    std::vector<Component*> rest =  rowRemove(lastRow, 0, lastRow->childrenCount(), false);
//    rowInsert(baseRow, begin_pos, std::move(rest), false);

//    for(auto row : rows)
//        delete row;

//    container->updateGeometry(notifyParent);

//    qDebug() << "ComponentEditor: removed.";

//    return removed;
//}



// Utils
void ComponentEditor::swap(Node *parent, Component *child, Component *by)
{
    int pos = parent->find(child);

    assert(pos >= 0);

    // Check if child is not already detached
    if(child->getParent() != nullptr)
        parent->detach(child);

    parent->mChildren[static_cast<size_t>(pos)] = by;
    parent->attach(by);
}

