#ifndef FRACTION_H
#define FRACTION_H

#include "Node.h"

class Fraction : public Node
{

public:
    explicit Fraction(std::vector<Component*> &&children);
    Component* clone();

public:
    void accept(ComponentVisitor& visitor);

public:
    void paintEvent(QPaintEvent *pe);

protected:
    inline void measure(Geometry &geometry);

private:
    QLineF mFractionLine;

};

#endif // FRACTION_H
