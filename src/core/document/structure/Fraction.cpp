#include <QDebug>
#include <QPaintEvent>
#include <QPainter>

#include "Fraction.h"
#include "ComponentVisitor.h"

#define NOMINATOR 0
#define DENOMINATOR 1

Fraction::Fraction(std::vector<Component*> &&children):
    Node(Component::Type::Fraction, std::move(children))
{
    assert(mChildren.size() == 2);
    mChildren.front()->setBaseScale(80);
    mChildren.back()->setBaseScale(80);
    updateGeometry(false);
}

Component *Fraction::clone()
{
    return new Fraction(cloneChildren());
}

void Fraction::accept(ComponentVisitor &visitor)
{
    visitor.visitFraction(this);
}

void Fraction::paintEvent(QPaintEvent *pe)
{
    pe->accept();
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, getScale() < 100);
    double w = 2.0*getScale()*0.01;
//    double w = 1.0;
    p.setPen(QPen(Qt::black, w));
    p.drawLine(mFractionLine);
    p.end();
}

void Fraction::measure(Geometry &geometry)
{
    Component* numerator = mChildren.at(0);
    Component* denominator = mChildren.at(1);

    int d = 1;
    int separatorWidth = 2;

    int width = 2*d + std::max(numerator->width(), denominator->width());
    int height = numerator->height() + denominator->height() + 4*d + separatorWidth;

    int center_x = width/2;
    int y = d;
    numerator->move(center_x - numerator->width()/2, y);
    y += 2*d + separatorWidth + numerator->height();
    denominator->move(center_x - denominator->width()/2, y);

    mFractionLine.setLine(0, mChildren.back()->y() - 2,
                          width, mChildren.back()->y() - 2);

    geometry.width = width;
    geometry.height = height;
    geometry.ascent = height/2;
    geometry.descent = height/2;
    geometry.leftBearing = 0;
    geometry.horizontalAdvance = width;
}
