#ifndef ROW_H
#define ROW_H

#include "Node.h"


class Row : public Node
{

    friend class ComponentEditor;

public:
    explicit Row(std::vector<Component*> &&children = {});
    ~Row();
    Component* clone();

    inline uint childrenCount() const { return static_cast<uint>(mChildren.size()); }
    inline const Component* getChild(const uint& at) const { return mChildren.at(at); }

    inline bool isExpandable() const { return mIsExpandable; }
    void setExpandable(bool expandable) { mIsExpandable = expandable; }


public:
    void accept(ComponentVisitor& visitor);

public:
    void keyPressEvent(QKeyEvent *ke);
    void mousePressEvent(QMouseEvent* mpe);
    void mouseMoveEvent(QMouseEvent* mpe);
    void mouseReleaseEvent(QMouseEvent* mpe);
    void paintEvent(QPaintEvent* pe);

protected:
    inline void measure(Geometry &geometry);

private:
    bool mIsExpandable = true;

};

#endif // ROW_H
