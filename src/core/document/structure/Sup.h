#ifndef SUP_H
#define SUP_H

#include "Node.h"


class Sup : public Node
{

public:
    explicit Sup(std::vector<Component*> &&children);
    Component* clone();

public:
    void accept(ComponentVisitor& visitor);

protected:
    inline void measure(Geometry &geometry);

};
#endif // SUP_H
