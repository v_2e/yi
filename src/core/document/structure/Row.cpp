#include <QPainter>
#include <QPaintEvent>
#include <QDebug>

#include "Row.h"
#include "Context.h"
#include "ComponentVisitor.h"

Row::Row(std::vector<Component*> &&children):
    Node(Component::Type::Row, std::move(children))
{
    updateGeometry();
}

Row::~Row()
{

}

Component *Row::clone()
{
    return new Row(cloneChildren());
}

void Row::accept(ComponentVisitor &visitor)
{
    visitor.visitRow(this);
}

void Row::keyPressEvent(QKeyEvent *ke)
{
    if(getContext() != nullptr)
        getContext()->onKeyPress(ke);
}

void Row::mousePressEvent(QMouseEvent *mpe)
{
    if(getContext() != nullptr)
        getContext()->onMousePress(this, mpe);
}

void Row::mouseMoveEvent(QMouseEvent *mpe)
{
    if(getContext() != nullptr)
        getContext()->onMouseMove(this, mpe);
}

void Row::mouseReleaseEvent(QMouseEvent *mpe){
    if(getContext() != nullptr)
        getContext()->onMouseRelease(this, mpe);
}

void Row::paintEvent(QPaintEvent *pe)
{
    pe->accept();
    QPainter p(this);
//    drawBoundingRect(p, Qt::green);
//    drawBaseLine(p, Qt::green);
}

void Row::measure(Geometry &geometry)
{
    const uint& n = childrenCount();
    if(n == 0){
        geometry.width = 20;
        geometry.height = 40;
        geometry.ascent = 20;
        geometry.descent = 20;
        geometry.leftBearing = 0;
        geometry.horizontalAdvance = 20;
        return;
    }else if(n == 1){
        Component* child = mChildren.at(0);
        geometry.width = child->width();
        geometry.height = child->height();
        geometry.ascent = child->ascent();
        geometry.descent = child->descent();
        geometry.leftBearing = child->leftBearing();
        geometry.horizontalAdvance = child->horizontalAdvance();
        child->move(0, 0);
        return;
    }

    // 1. find max ascend
    int ascent = 0;
    int descent = 0;
    for(auto c : mChildren){
        ascent = std::max(ascent, c->ascent());
        descent = std::max(descent, c->descent());
    }

    int d = 2;
    const int lb = mChildren.size() > 0 ? mChildren.front()->leftBearing() : 0;
    int x = - lb;
    int h = 22;
    int advance = 0;
    for(auto c : mChildren){
        c->move(x + c->leftBearing(), ascent - c->ascent());
        x += c->horizontalAdvance();
    }
    advance = x;
    h += 2*d;
    if(x < 22) x = 22;
    x = mChildren.back()->right() + d;

    geometry.ascent = ascent;
    geometry.descent = descent;
    geometry.leftBearing = lb;
    geometry.horizontalAdvance = lb + advance;
    geometry.width = x;
    geometry.height = ascent + descent;
}
