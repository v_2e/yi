#ifndef CONTAINER_H
#define CONTAINER_H

#include "Node.h"
#include "Row.h"

#include <QPainter>

class Container : public Node
{

    friend class ComponentEditor;

public:
    explicit Container();
    explicit Container(std::vector<std::vector<Component*>> &&children);
    explicit Container(std::vector<Row*> &&rows);
    explicit Container(Row* getRow);
    ~Container();
    Component* clone();

    inline uint rowsCount() const {
        return static_cast<uint>(mChildren.size());
    }

    inline Row* getRow(const uint &line) const { return static_cast<Row*>(mChildren.at(line)); }
    inline Row* frontRow() const { return static_cast<Row*>(mChildren.front()); }
    inline Row* backRow() const { return static_cast<Row*>(mChildren.back()); }

public:
    void accept(ComponentVisitor& visitor);


public:
    void paintEvent(QPaintEvent* pe);
    void keyPressEvent(QKeyEvent *ke);
    void mousePressEvent(QMouseEvent* mpe);
    void mouseMoveEvent(QMouseEvent* mpe);
    void mouseReleaseEvent(QMouseEvent* mpe);

protected:
    void measure(Geometry &geometry);
    void insertLine(uint at, std::vector<Component*> &&components = {});

};

#endif // CONTAINER_H
