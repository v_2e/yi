#include <QPaintEvent>
#include <QPainter>

#include "Container.h"
#include "Context.h"
#include "ComponentVisitor.h"

Container::Container():
    Node(Component::Type::Container)
{
//    insertLine(0);
    updateGeometry();
}

Container::Container(std::vector<std::vector<Component*>> &&children):
    Node(Component::Type::Container)
{
    const size_t n = children.size();
    mChildren = std::vector<Component*>(n);
    for(size_t i = 0; i < n; i++){
        Row* row = new Row(std::move(children.at(i)));
        mChildren[i] = row;
        attach(row);
    }
    if(n == 0)
        insertLine(0);
    updateGeometry();
}

Container::Container(std::vector<Row*> &&rows):
    Node(Component::Type::Container)
{
    const size_t n = rows.size();
    if(n != 0){
        mChildren.resize(n);
        for(size_t i = 0; i < n; i++){
            Row* row = rows[i];
            mChildren[i] = row;
            attach(row);
        }
    }else{
        insertLine(0);
    }
    updateGeometry();
}

Container::Container(Row *row):
    Node(Component::Type::Container)
{
    mChildren.push_back(row);
    attach(row);
    updateGeometry(false);
}

Container::~Container()
{

}

Component *Container::clone()
{
    std::vector<Component*> children = cloneChildren();
    const size_t n = children.size();
    std::vector<Row*> rows(n);
    for(size_t i = 0; i < n; i++)
        rows[i] = static_cast<Row*>(children[i]);
    return new Container(std::move(rows));
}

void Container::accept(ComponentVisitor &visitor)
{
    visitor.visitContainer(this);
}

void Container::insertLine(uint at, std::vector<Component*> &&components)
{
    assert(at <= mChildren.size());
    Row* row = new Row(std::move(components));
    mChildren.insert(mChildren.begin() + at, row);
    attach(row);
}

void Container::paintEvent(QPaintEvent *pe)
{
    pe->accept();
    QPainter p(this);
//    drawBoundingRect(p, Qt::gray);
}

void Container::keyPressEvent(QKeyEvent *ke)
{
    if(getContext() != nullptr)
        getContext()->onKeyPress(ke);
}

void Container::mousePressEvent(QMouseEvent *mpe)
{
    if(getContext() != nullptr)
        getContext()->onMousePress(this, mpe);
}

void Container::mouseMoveEvent(QMouseEvent *mpe)
{
    if(getContext() != nullptr)
        getContext()->onMouseMove(this, mpe);
}

void Container::mouseReleaseEvent(QMouseEvent *mpe)
{
    if(getContext() != nullptr)
        getContext()->onMouseRelease(this, mpe);
}

void Container::measure(Geometry &geometry)
{
    const uint& n = rowsCount();
    if(n == 1){
        Component* line = mChildren.at(0);
        line->move(0, 0);
        geometry.width = line->width();
        geometry.height = line->height();
        geometry.ascent = line->ascent();
        geometry.descent = line->descent();
        geometry.leftBearing = line->leftBearing();
        geometry.horizontalAdvance = line->horizontalAdvance();
    }else{
        int d = 0;
        int w = 20;
        int h = d;
        for(auto c : mChildren){
            c->move(d, h);
            h += c->height() + d;
            w = std::max(w, c->width());
        }
        if(h < 40) h = 40;

        geometry.ascent = h / 2;
        geometry.descent = h / 2;
        geometry.width = w;
        geometry.horizontalAdvance = w;
        geometry.leftBearing = 0;
        geometry.height = h;
    }



}
