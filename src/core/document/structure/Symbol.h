#ifndef SYMBOL_H
#define SYMBOL_H

#include "Component.h"
#include "GlyphDrawable.h"
#include "IGlyphProvider.h"

class Symbol : public Component
{

public:

    struct Style{
        std::string font_family;
        int font_size;
        bool font_italic;
    };

public:
    explicit Symbol(Component::Type type, IGlyphProvider* glyphDrawableProvider, char32_t c);
    ~Symbol();
    Component* clone();
    char32_t getData();

public:
    void accept(ComponentVisitor& visitor);


public:
    void paintEvent(QPaintEvent* pe);

protected:
    inline void measure(Geometry &geometry);

private:
    IGlyphProvider* mDrawableProvider;
    GlyphDrawableHandle* mDrawableHandle;

    char32_t mSymbolCode;
    GlyphDrawable* mGlyphDrawable = nullptr;
    QPointF mGlyphDrawPoint;

};

#endif // SYMBOL_H
