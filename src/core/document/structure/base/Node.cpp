#include "Node.h"

Node::Node(Type type, std::vector<Component*> &&children):
    Component (type),
    mChildren(std::move(children))
{
    for(auto c : mChildren)
        attach(c);
}

Node::~Node()
{
    for(auto child : mChildren)
        delete child;
}

void Node::setContext(Context *context)
{
    Component::setContext(context);
    for(auto child : mChildren)
        child->setContext(context);
}

bool Node::setScale(int scalePrc, bool remeasure)
{
    bool changed = Component::setScale(scalePrc, false);
    if(changed){
        const int scale = getScale();
        for(auto child : mChildren)
            child->setScale(scale, true);
        if(remeasure)
            updateGeometry(false);
        return true;
    }
    return false;
}

void Node::attach(Component *component)
{
    component->setParent(this);
}

void Node::detach(Component *component)
{
    component->setParent(nullptr);
}

std::vector<Component*> Node::cloneChildren()
{
    std::vector<Component*> c(mChildren.size());
    for(size_t i = 0; i < mChildren.size(); i++)
        c[i] = mChildren.at(i)->clone();
    return c;
}
