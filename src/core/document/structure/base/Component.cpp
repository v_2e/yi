#include "Component.h"
#include "Node.h"

Component::Component(Type type):
    mType(type)
{

}

Component::~Component()
{
}

void Component::setParent(Node *parent)
{
    mParent = parent;
    View::setParent(parent);
}
