#ifndef COMPONENTVISITOR_H
#define COMPONENTVISITOR_H

class Row;
class Container;
class Symbol;
class Fraction;
class Sup;

class ComponentVisitor
{
public:
    virtual ~ComponentVisitor() = 0;
    virtual void visitRow(Row *row) = 0;
    virtual void visitContainer(Container *container) = 0;
    virtual void visitSymbol(Symbol *symbol) = 0;
    virtual void visitFraction(Fraction *fraction) = 0;
    virtual void visitSup(Sup *sup) = 0;
};

#endif // COMPONENTVISITOR_H
