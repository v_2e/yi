#ifndef COMPONENT_H
#define COMPONENT_H

#include "View.h"

typedef unsigned int uint;

class Node;
class Context;
class TeXStream;
class MMLStream;
class ComponentVisitor;

class Component : public View
{

    friend class Node;

public:

    enum class Type{
        Symbol,
        Operator,
        Row,
        Container,
        Fraction,
        Fences,
        Root,
        SquareRoot,
        Sub,
        Sup,
        SubSup,
        SubSupR,
        Over,
        Under,
        OverUnderR,
        Table
    };

    explicit Component(Type type);
    virtual ~Component() = 0;
    virtual Component* clone() = 0;

    inline Type getType() const {
        return mType;
    }

    inline Node* getParent(){
        return mParent;
    }

public:
    virtual void accept(ComponentVisitor& visitor) = 0;

protected:
    void setParent(Node* parent);

private:
    Node* mParent;
    Type mType;

};

#endif // COMPONENT_H
