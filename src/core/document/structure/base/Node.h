#ifndef NODE_H
#define NODE_H

#include "Component.h"

class Node : public Component
{

    friend class ComponentEditor;

public:
    Node(Type type, std::vector<Component*> &&children = {});
    virtual ~Node();

    inline uint componentsCount() { return static_cast<uint>(mChildren.size()); }
    Component* getComponent(uint pos) { return mChildren.at(pos); }

    inline int find(const Component* component) const{
        int i = 0;
        for(auto c : mChildren){
            if(c == component) return i;
            i++;
        }
        return -1;
    }

    // TODO: implement children iterator


protected:
    void setContext(Context* context);
    bool setScale(int scalePrc, bool remeasure = true);
    void attach(Component* component);
    void detach(Component* component);

    std::vector<Component*> cloneChildren();

protected:
    std::vector<Component*> mChildren;

};

#endif // NODE_H
