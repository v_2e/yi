#ifndef ROOTNODE_H
#define ROOTNODE_H

#include "Container.h"

class RootNode : public Container
{

    friend class Document;

private:
    RootNode(std::vector<std::vector<Component*>> &&content = {{}});
    inline void measure(Geometry &geometry);

};

#endif // ROOTNODE_H
