#include "Document.h"

Document::Document(std::vector<std::vector<Component*>> &&content):
    mRoot(std::move(content)),
    mEditor(this)
{
    mEditor.setCursor(mRoot.getRow(0), 0);
}

void Document::attach(Context *context)
{
    assert(!mIsAttached);
    assert(context != nullptr);

    mRoot.setContext(context);
    mIsAttached = true;
}

void Document::detach()
{
    mRoot.setContext(nullptr);
    mIsAttached = false;
}
