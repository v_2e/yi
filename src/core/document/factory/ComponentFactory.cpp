#include "ComponentFactory.h"

#include<QString>
#include <QDebug>
#include <unordered_map>

#include "Node.h"
#include "Symbol.h"
#include "Row.h"
#include "Container.h"
#include "Fraction.h"
#include "Sup.h"



ComponentFactory::ComponentFactory(IGlyphProvider* glyphProvider):
    mGlyphProvider(glyphProvider)
{
    registerNodeTypes();
}


template <class T>
Node* create(std::vector<Component*> &&children){
    return new T(std::move(children));
}

void ComponentFactory::registerNodeTypes()
{
    mNodeCreators[Component::Type::Row] = &create<Row>;
    mNodeCreators[Component::Type::Fraction] = &create<Fraction>;
    mNodeCreators[Component::Type::Sup] = &create<Sup>;
}

Component *ComponentFactory::createSymbol(Component::Type type, char32_t c)
{
    return new Symbol(type, mGlyphProvider, c);
}

Component *ComponentFactory::createIdentifier(char32_t c)
{
    return new Symbol(Component::Type::Symbol, mGlyphProvider, c);
}

Component *ComponentFactory::createNode(Component::Type type, std::vector<Component*> &&children)
{
        assert(type != Component::Type::Symbol &&
               type != Component::Type::Operator &&
               type != Component::Type::Container);

        try {
            NodeCreator &creator = mNodeCreators.at(type);
            return creator(std::move(children));
        } catch (std::out_of_range *e) {
            Q_UNUSED(e)
            qDebug() << "ComponentFactory: type is not registered.";
            // return new MockStaticView(node, children_views);
            return nullptr;
        }
}

Component *ComponentFactory::createContainer(std::vector<std::vector<Component*>> &&children)
{
    const size_t n = children.size();
    std::vector<Row*> rows(n);
    for(size_t i = 0; i < n; i++)
        rows[i] = new Row(std::move(children.at(i)));
    return new Container(std::move(rows));
}
