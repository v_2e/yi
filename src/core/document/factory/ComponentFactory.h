#ifndef VIEWBUILDER_H
#define VIEWBUILDER_H

#include "Component.h"
#include <unordered_map>

class IGlyphProvider;

class Symbol;
class Node;

class ComponentFactory
{

public:
    ComponentFactory(IGlyphProvider* glyphProvider);
    Component* createSymbol(Component::Type type, char32_t c);
    Component* createIdentifier(char32_t c);
    Component* createNode(Component::Type type, std::vector<Component*> &&children);
    Component* createContainer(std::vector<std::vector<Component*>> &&children = {{}});

private:
    void registerNodeTypes();

    typedef Node* (*NodeCreator) (std::vector<Component*> &&);
    std::unordered_map<Component::Type, NodeCreator> mNodeCreators;

private:
    IGlyphProvider* mGlyphProvider;
};

#endif // VIEWBUILDER_H
