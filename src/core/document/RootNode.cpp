#include "RootNode.h"

#include <QtDebug>
#include <QElapsedTimer>

#include <QPaintEvent>
#include <QPainter>

RootNode::RootNode(std::vector<std::vector<Component*>> &&content):
    Container(std::move(content))
{
//    setBaseScale(200);
}

void RootNode::measure(Geometry &geometry)
{
//    QElapsedTimer t;
//    t.start();
    Container::measure(geometry);
//    qDebug() << "Measured in " << t.elapsed() << " ms.";
}

