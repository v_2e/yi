#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "RootNode.h"
#include "DocumentEditor.h"

class Context;

class Document
{

public:
    explicit Document(std::vector<std::vector<Component*>> &&content = {{}});

    DocumentEditor& editor() { return mEditor; }
    RootNode& root() { return mRoot; }

    void attach(Context* context);
    void detach();

private:
    RootNode mRoot;
    DocumentEditor mEditor;

    bool mIsAttached = false;

};

#endif // DOCUMENT_H
