#ifndef CONTEXT_H
#define CONTEXT_H

#include <QKeyEvent>

class Component;
class QPoint;

class Context
{
public:
    Context();
    virtual ~Context() = 0;
    virtual void onKeyPress (QKeyEvent* ke) = 0;
    virtual void onMousePress (Component* container, QMouseEvent* event) = 0;
    virtual void onMouseMove(Component* container, QMouseEvent* event) = 0;
    virtual void onMouseRelease(Component* container, QMouseEvent* event) = 0;
};

#endif // CONTEXT_H
