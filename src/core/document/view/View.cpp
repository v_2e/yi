#include "View.h"

#include "Node.h"

#include <QDebug>


View::View() : QWidget(nullptr)
{

}

View::~View()
{

}

QPoint View::globalPos() const
{
    QPoint gpos(0, 0);
    View const* t = this, *p = mParent;
    while (p != nullptr) {
        gpos += t->pos();
        t = p;
        p = p->mParent;
    }
    return gpos;
}

void View::setParent(View* parent)
{
    mParent = parent;
    if(parent != nullptr){
        setContext(parent->mContext);
        setScale(parent->mScalePrc, false /* update geometry if scale changed */);
        updateGeometry(false);


        // inherit scale, etc...

        // then finally set context
        // which cause geometry update without notifying the parent

//        int scale = qRound(parent->mScalePrc * mBaseScalePrc * 0.00001f);
//        if(scale != mScalePrc)

        //        if(parent->getScale() != 1.0f)
        //            setScale(getScale()*parent->getScale());

//        if(parent->mContext != nullptr)
//            setContext(parent->mContext);

        QWidget::setParent(parent);
        if(isHidden())
            show();
    }else{
        if(!isHidden())
            hide();
        QWidget::setParent(parent);
    }
}

void View::setContext(Context *context)
{
    mContext = context;
//    updateGeometry(false);
}

bool View::setScale(int scalePrc, bool remeasure)
{
    int normalized = qRound(scalePrc * mBaseScalePrc * 0.01f);
    if(normalized < 30)
        normalized = 30;
    if(normalized != mScalePrc){
        mScalePrc = normalized;
        if(remeasure)
            updateGeometry(false);
        return true;
    }
    return false;
}

bool View::setBaseScale(int scalePrc)
{
    if(scalePrc < 40)
        scalePrc = 40;
    if(scalePrc != mBaseScalePrc){
        float parentSetScale = mScalePrc*(100.0f / mBaseScalePrc);
        mBaseScalePrc = scalePrc;
        setScale(qRound(parentSetScale), true);
        return true;
    }
    return false;
}

int View::getBaseScale()
{
    return mBaseScalePrc;
}

int View::getScale() const
{
    return mScalePrc;
}

void View::drawBoundingRect(QPainter &p, QColor color)
{
    p.save();
    p.setPen(QPen(color, 1));
    QRect r(0, 0, width() - 1, height() - 1);
    p.drawRect(r);
    p.restore();
}

void View::drawBaseLine(QPainter &p, QColor color, double thickness)
{
    p.save();
    p.setPen(QPen(color, thickness));
    QLineF baseline(0, ascent(), width() - 1, ascent());
    p.drawLine(baseline);
    p.restore();
}

void View::updateGeometry(bool notifyParent)
{
    measure(mGeometry);
    if(mGeometry.width != width() || mGeometry.height != height()){
        setFixedSize(mGeometry.width, mGeometry.height);
        View* const parent = mParent;
        if(notifyParent && parent != nullptr)
            parent->updateGeometry();
    }
}


