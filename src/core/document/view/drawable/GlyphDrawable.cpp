#include "GlyphDrawable.h"

#include <QPainter>
#include <QtDebug>

#include <QFont>
#include <QFontMetrics>

inline int ceil(const double &x){
    return x - int(x) > 0.0 ? int(x) : int(x) + 1;
}

GlyphDrawable::GlyphDrawable(const char32_t &c, QRawFont *rawFont) :
    mChar(QString::fromUcs4(&c, 1)),
    mRawFont(rawFont)
{
    mGlyphRun.setRawFont(*mRawFont);
    initialize();
}

void GlyphDrawable::initialize()
{
    QVector<quint32> indices = mRawFont->glyphIndexesForString(QString(mChar));
    mGlyphRect = mRawFont->boundingRect(indices.first());
    mGlyphAdvances = mRawFont->advancesForGlyphIndexes(indices).first();

    mGlyphRun.setGlyphIndexes(indices);
    mGlyphRun.setPositions({QPointF(0, 0)});

    mAscent = qRound(-mGlyphRect.y());
    mDescent = qRound(mGlyphRect.height() + mGlyphRect.y() + 2);
    mLeftBearing = qRound(mGlyphRect.left());
    mHorizontalAdvance = qRound(mGlyphAdvances.x());
    mWidth = ceil(mGlyphRect.width());
    mHeight = ceil(mGlyphRect.height());


    qDebug() << " ";
    qDebug() << " ";
    qDebug() << "Glyph test: " << (mRawFont->familyName() + QString(" %1").arg(qRound(mRawFont->pixelSize())));
    qDebug() << "Char: " << mChar;
    qDebug() << "Ascent: " << mRawFont->ascent();
    qDebug() << "Descent: " << mRawFont->descent();
    qDebug() << "Left bearing: " << leftBearing();
    qDebug() << "Horizontal advance: " << horizontalAdvance();
    qDebug() << "Width: " << width();
    qDebug() << "Rect: " << mGlyphRect;


    // Test subsup offset
//    QPainterPath path = mRawFont->pathForGlyph(indices.at(0));
//    double height = mGlyphRect.height();
//    double xHeight = mRawFont->xHeight();

//    QRectF botRect(mGlyphRect.right() + 1, 0, 4, xHeight);
//    QRectF topRect = botRect;
//    topRect.moveBottom(-(xHeight/2 + 1));
//    botRect.moveTop(-(xHeight/2 - 1));

//    while (!path.intersects(topRect)) {
//        topRect.translate(-1.0, 0.0);
//    }
//    while (!path.intersects(botRect)) {
//        botRect.translate(-1.0, 0.0);
//    }

//    mTestTopRect = topRect;
//    mTestBottomRect = botRect;
//    mPath = path;


}

int GlyphDrawable::ascent() const
{
    return mAscent;
}

int GlyphDrawable::descent() const
{
    return mDescent;
}

int GlyphDrawable::leftBearing() const
{
    return mLeftBearing;
}

int GlyphDrawable::horizontalAdvance() const
{
    return mHorizontalAdvance;
}

int GlyphDrawable::width() const
{
    return mWidth;
}

int GlyphDrawable::height() const
{
    return mHeight;
}

void GlyphDrawable::draw(const QPointF &point, QPainter &painter)
{
//    painter.save();
//    painter.setPen(QPen(Qt::red, 1));
////    painter.translate(point);
//    painter.translate( - mGlyphRect.x(), - mGlyphRect.y());
////    painter.drawLine(-1000, 0, 1000, 0);
////    painter.drawLine(0, -1000, 0, 1000);

////    QRectF r(1, 1, mGlyphRect.width() - 1, mGlyphRect.height() - 1);
////    r.translate(0, -ascent());
////    painter.drawRect(r);


//    painter.drawPath(mPath);

//    painter.drawRect(mTestTopRect);
//    painter.drawRect(mTestBottomRect);
////    QRectF r = mTestTopRect.translated(point.x(), point.y());
////    painter.drawRect(r);
//    painter.restore();

    painter.drawGlyphRun(point, mGlyphRun);
//    painter.drawText(point, mChar);
}


// Qt bug?
//    qDebug() << "Start initializing glyph data";
//    quint32 glyph_index;
//    int n_indices[2];
//    bool result;
//    while(!(result = mRawFont->glyphIndexesForChars(&mChar, 1, &glyph_index, n_indices))){
//        qDebug() << "Bad indices count: " << n_indices;
//    }
//    qDebug() << "Glyph index: " << glyph_index << " " << n_indices << result;

//    mRawFont->advancesForGlyphIndexes(&glyph_index, &mGlyphAdvances, n_indices[0]);
//    mGlyphRect = mRawFont->boundingRect(glyph_index);


//    mGlyphRun.setGlyphIndexes(QVector<quint32>({glyph_index}));
//    mGlyphRun.setPositions({QPointF(0, 0)});
//    mGlyphRun.setBoundingRect(mGlyphRect);
