#include "IGlyphProvider.h"

IGlyphProvider::~IGlyphProvider()
{

}

int IGlyphProvider::incrementUseCounter(GlyphDrawableHandle *handle)
{
    return ++handle->use_counter;
}

int IGlyphProvider::decrementUseCounter(GlyphDrawableHandle *handle)
{
    return --handle->use_counter;
}
