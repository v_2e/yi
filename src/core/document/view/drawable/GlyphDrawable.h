#ifndef GLYPHDRAWABLE_H
#define GLYPHDRAWABLE_H

#include <QRawFont>
#include <QGlyphRun>

class QPainter;

class GlyphDrawable
{

public:
    explicit GlyphDrawable(const char32_t& c, QRawFont* rawFont);
    int ascent() const;
    int descent() const;
    int leftBearing() const;
    int horizontalAdvance() const;
    int width() const;
    int height() const;
    void draw(const QPointF& point, QPainter& painter);

private:
    void initialize();

private:
    QString mChar;
    QRawFont* mRawFont;
    QGlyphRun mGlyphRun;


    int mAscent;
    int mDescent;
    int mLeftBearing;
    int mHorizontalAdvance;
    int mWidth;
    int mHeight;



    QRectF mGlyphRect;
    QPointF mGlyphAdvances;

    QRectF mTestTopRect;
    QRectF mTestBottomRect;
    QPainterPath mPath;

};

#endif // GLYPHDRAWABLE_H
