#ifndef IGLYPHPROVIDER_H
#define IGLYPHPROVIDER_H

#include <string>
#include <QString>

class GlyphDrawable;

struct GlyphDrawableHandle{
public:
    QString id;
    GlyphDrawable* drawable;
private:
    int use_counter = 0;

    friend class IGlyphProvider;
};

class IGlyphProvider
{

public:

    struct Request{
        char32_t glyph_code;
        std::string font_family = "default";
        int font_size = 12;
    };

    virtual ~IGlyphProvider() = 0;
    virtual GlyphDrawableHandle* provideDrawable(const Request &request) = 0;
    virtual void recycleDrawable(GlyphDrawableHandle* handle) = 0;

protected:
    int incrementUseCounter(GlyphDrawableHandle* handle);
    int decrementUseCounter(GlyphDrawableHandle* handle);

};

#endif // IGLYPHPROVIDER_H
