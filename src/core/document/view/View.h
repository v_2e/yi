#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include <QGraphicsView>

class Node;
class Context;

struct Geometry{
    int width;
    int height;
    int ascent;
    int descent;
    int leftBearing;
    int horizontalAdvance;
};

class View : public QWidget
{

    Q_OBJECT

public:
    explicit View();
    virtual ~View() = 0;

    inline Context* getContext(){
        return mContext;
    }

    void updateGeometry(bool notifyParent = true);
    inline int leftBearing() const { return mGeometry.leftBearing; }
    inline int horizontalAdvance() const { return mGeometry.horizontalAdvance; }
    inline int ascent() const { return mGeometry.ascent; }
    inline int descent() const { return mGeometry.descent; }
    inline int centerX() const { return x() + (mGeometry.width >> 1); }
    inline int centerY() const { return y() + (mGeometry.height >> 1); }
    inline int left() const { return x(); }
    inline int top() const { return y(); }
    inline int right() const { return x() + mGeometry.width; }
    inline int bottom() const { return y() + mGeometry.height; }
    QPoint globalPos() const;
    int getBaseScale();
    int getScale() const;

    // Temporary public ?
    bool setBaseScale(int scalePrc);

protected:
    void setParent(View* node);
    virtual void setContext(Context* context);
    virtual bool setScale(int scalePrc, bool remeasure = true);
    void notifyResized();

protected:
    inline virtual void measure(Geometry &geometry) = 0;

private:
    View* mParent = nullptr;
    Context* mContext = nullptr;
    Geometry mGeometry;

    // Scale logic
    int mBaseScalePrc = 100;
    int mScalePrc = 100;

// Debugging
protected:
    void drawBoundingRect(QPainter &p, QColor color);
    void drawBaseLine(QPainter &p, QColor color, double thickness = 1.0);

};

#endif // VIEW_H
