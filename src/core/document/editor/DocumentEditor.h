#ifndef DOCUMENTEDITOR_H
#define DOCUMENTEDITOR_H

#include <QObject>

#include "ComponentEditor.h"
#include "SelectionController.h"
#include "ActionFactory.h"
#include "ActionManager.h"
#include "Cursor.h"
#include "Selection.h"
#include "Container.h"

class Document;

class DocumentEditor : public QObject
{

    Q_OBJECT

    friend class Document;

private:
     explicit DocumentEditor(Document* document);
     ~DocumentEditor();

public:
    inline const Cursor& cursor() const { return mCursor; }
    void setCursor(Row *row, const uint &pos);



    inline const Selection& selection() const { return mSelection; }
    void setSelection(Container* container,
                      const uint & begin_line, const uint & begin_pos,
                      const uint & end_line, const uint & end_pos,
                      const bool& reverse = false);
    void clearSelection();


    void insert(Row* where, const uint &pos,
                Component* component);
    void insert(Row* where, const uint &pos,
                std::vector<std::vector<Component*>> &&components);
    void remove(Row* where, const uint &at);
    void remove(Container* where,
                const uint &begin_line, const uint &begin_pos,
                const uint &end_line, const uint &end_pos);

    // Undo/Redo
    bool canUndo() const;
    void undo();
    bool canRedo() const;
    void redo();
    void setUndoStackSize(const long unsigned int& size);

signals:
    void documentChanged();

private:
    void notifyDocumentChanged();

private:
    Document *mDocument = nullptr;
    Cursor mCursor;
    Selection mSelection;
    ComponentEditor mComponentEditor;
    SelectionController mSelectionController;
    ActionFactory mActionFactory;
    ActionManager mActionManager;


};

#endif // DOCUMENTEDITOR_H
