#ifndef CURSOR_H
#define CURSOR_H

#include <QObject>
class Row;

typedef unsigned int uint;

class Cursor : public QObject
{

    Q_OBJECT

    friend class SelectionController;

public:
    Cursor();
    ~Cursor();

    inline Row* focus() const { return mFocus; }
    inline uint pos() const { return mPos; }

signals:
    void changed(const Cursor& cursor);

private:
    Row* mFocus = nullptr;
    uint mPos = 0;

};

#endif // CURSOR_H
