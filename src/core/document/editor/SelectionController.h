#ifndef SELECTIONCONTROLLER_H
#define SELECTIONCONTROLLER_H

#include "Selection.h"
#include "Cursor.h"

class SelectionController
{

    friend class DocumentEditor;

private:
    explicit SelectionController(Selection &selection,
                                 Cursor &cursor);

public:
    void setCursor(Row* row, const uint& pos);
    void setSelection(Container* container,
                      const uint & begin_line, const uint & begin_pos,
                      const uint & end_line, const uint & end_pos,
                      const bool& reverse = false);

private:
    void notifyCursorChanged();
    void notifySelectionChanged();

private:
    Selection& selection;
    Cursor& cursor;

};

#endif // SELECTIONCONTROLLER_H
