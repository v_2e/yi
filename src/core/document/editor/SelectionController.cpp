#include "SelectionController.h"

#include "Row.h"
#include "Container.h"

SelectionController::SelectionController(Selection &selection,
                                         Cursor &cursor):
    selection(selection),
    cursor(cursor)
{

}


void SelectionController::setCursor(Row *row, const uint &pos)
{
    assert(row != nullptr);
    assert(pos <= row->childrenCount());

    cursor.mFocus = row;
    cursor.mPos = pos;
    row->setFocus();

    notifyCursorChanged();
}

void SelectionController::setSelection(Container *container,
                                       const uint &begin_line, const uint &begin_pos,
                                       const uint &end_line, const uint &end_pos,
                                       const bool& reverse)
{
    if(container != nullptr){
        assert(begin_line < container->rowsCount());
        assert(end_line < container->rowsCount());
        assert(begin_pos <= container->getRow(begin_line)->childrenCount());
        assert(end_pos <= container->getRow(end_line)->childrenCount());
        assert(begin_line < end_line || begin_pos <= end_pos);

        selection.mHolder = container;
        selection.mBeginLine = begin_line;
        selection.mBeginPos = begin_pos;
        selection.mEndLine = end_line;
        selection.mEndPos = end_pos;
        selection.mReverse = reverse;

    }else{
        selection.mHolder = nullptr;
        selection.mBeginLine = 0;
        selection.mBeginPos = 0;
        selection.mEndLine = 0;
        selection.mEndPos = 0;
        selection.mReverse = false;
    }

    notifySelectionChanged();
}

void SelectionController::notifyCursorChanged()
{
    emit cursor.changed(cursor);
}

void SelectionController::notifySelectionChanged()
{
    emit selection.changed(selection);
}
