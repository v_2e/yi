#include "InflateRowAction.h"

#include "InsertMultipleAction.h"


InflateRowAction::InflateRowAction(ComponentEditor &editor,
                                           SelectionController &selectionController,
                                           Row *row,
                                           const uint &at,
                                           std::vector<std::vector<Component*>> &&components):
    mEditor(editor),
    mSelectionController(selectionController),
    mInsertionRow(row),
    mInsertionPos(at),
    mNewCursorPos(0),
    mInsertComponents(std::move(components)),
    mCachedContainer(nullptr)
{
    assert(mInsertionRow != nullptr);
}

InflateRowAction::~InflateRowAction()
{
    for(auto line : mInsertComponents)
        for(auto component : line)
            delete component;
    if(mCachedContainer != nullptr)
        delete mCachedContainer;
}

void InflateRowAction::apply()
{
    Node* parent = mInsertionRow->getParent();

    assert(parent != nullptr);
    assert(parent->getType() != Component::Type::Container);

    Container* container;
    if(mCachedContainer == nullptr){
        container = new Container();
        const uint new_rows_count = static_cast<uint>(mInsertComponents.size() - 1);
        if(new_rows_count > 0){
            std::vector<Row*> rows(new_rows_count);
            for(uint i = 0; i < new_rows_count; i++)
                rows[i] = new Row(std::move(mInsertComponents.at(i + 1)));
            mInsertComponents.erase(mInsertComponents.begin() + 1,
                                    mInsertComponents.end());
            mEditor.insertRows(container, 0, std::move(rows), false);
        }
    }else{
        container = mCachedContainer;
        mCachedContainer = nullptr;
    }

    std::vector<Component*> rest = mEditor.remove(mInsertionRow, mInsertionPos,
                                                  mInsertionRow->childrenCount(),
                                                  false);
    mEditor.insert(mInsertionRow, mInsertionPos,
                  std::move(mInsertComponents.front()),
                  false);

    Row* last_row = container->backRow();
    const uint last_row_children_count = last_row->childrenCount();
    if(rest.size() > 0)
        mEditor.insert(last_row, last_row_children_count, std::move(rest), false);

    mEditor.replace(parent, mInsertionRow, container, false, false);
    mEditor.insertRow(container, 0, mInsertionRow, true);

    mInsertComponents.clear();
    mNewCursorPos = last_row_children_count;
    mSelectionController.setCursor(last_row, mNewCursorPos);
}

void InflateRowAction::discard()
{
    Node* parent = mInsertionRow->getParent();

    assert(parent != nullptr);
    assert(parent->getType() == Component::Type::Container);
    assert(mInsertComponents.size() == 0);

    Container* inflated = static_cast<Container*>(parent);
    Row* row = mEditor.removeRows(inflated, 0, 1, false).front();

    assert(row == mInsertionRow);

    if(mInsertionPos != row->childrenCount()){
        std::vector<Component*> components = mEditor.remove(row, mInsertionPos,
                                                            row->childrenCount());
        mInsertComponents.push_back(components);
    }

    Row* back = inflated->backRow();
    std::vector<Component*> rest = mEditor.remove(back,
                                                  mNewCursorPos,
                                                  back->childrenCount(), false);
    mEditor.insert(row, mInsertionPos, std::move(rest), false);
    mEditor.replace(inflated->getParent(), inflated, row, true, true);
    mCachedContainer = inflated;

    mNewCursorPos = mInsertionPos;
    mSelectionController.setCursor(row, mNewCursorPos);
}

