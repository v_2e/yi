#ifndef INFLATECONTAINERACTION_H
#define INFLATECONTAINERACTION_H

#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Row.h"
#include "Container.h"


class InflateRowAction : public Action
{
public:
    InflateRowAction(ComponentEditor &mEditor,
                         SelectionController &mSelectionController,
                         Row* row, const uint& at,
                         std::vector<std::vector<Component*>> &&components);
    ~InflateRowAction();

    void apply();
    void discard();

private:
    ComponentEditor &mEditor;
    SelectionController &mSelectionController;
    Row* mInsertionRow;
    uint mInsertionPos;
    uint mNewCursorPos;
    std::vector<std::vector<Component*>> mInsertComponents;

    Container* mCachedContainer;

};

#endif // INFLATECONTAINERACTION_H
