#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

#include <QObject>

#include "Action.h"
#include <deque>

class ActionManager : public QObject
{
    Q_OBJECT

    friend class DocumentEditor;

public:
    explicit ActionManager();
    void execute(Action* action);

    void setUndoStackSize(const unsigned long int& size);

    void undo();
    void redo();

    inline bool canUndo() const { return mUndoStack.size() > 0; }
    inline bool canRedo() const { return mRedoStack.size() > 0; }

signals:
    void executed();

private:
    unsigned long int mUndoStackSize = 100;
    std::deque<Action*> mUndoStack;
    std::deque<Action*> mRedoStack;

    void clear(std::deque<Action*> &deque);
    void trim(std::deque<Action*> &deque);
    void clearAction(Action* action);

};

#endif // ACTIONMANAGER_H
