#include "InsertMultipleAction.h"


InsertMultipleAction::InsertMultipleAction(ComponentEditor &editor,
                                           SelectionController &selectionController,
                                           Row *where,
                                           const uint &at,
                                           std::vector<std::vector<Component*>> &&what):
    mEditor(editor),
    mSelectionController(selectionController),
    mInsertionRow(where),
    mInsertionPos(at),
    mNewCursorPos(0),
    mInsertComponents(std::move(what))
{
    assert(mInsertionRow != nullptr);
    mInsertLinesCount = static_cast<uint>(mInsertComponents.size());
}

InsertMultipleAction::~InsertMultipleAction()
{
    for(auto line : mInsertComponents)
        for(auto component : line)
            delete component;
    for(auto row : mCachedRows)
        delete row;
}

void InsertMultipleAction::apply()
{
    Node* parent = mInsertionRow->getParent();

    assert(parent->getType() == Component::Type::Container);

    Container* container = static_cast<Container*>(parent);

    int pos_in_parent = container->find(mInsertionRow);
    assert(pos_in_parent >= 0);

    // remove the rest components of the insertion row
    // (i.e. the components after the insertion pos)
    std::vector<Component*> rest = mEditor.remove(mInsertionRow,
                                                 mInsertionPos,
                                                 mInsertionRow->childrenCount(),
                                                 false);


    // insert the first-line components inside the insertion row
    mEditor.insert(mInsertionRow, mInsertionPos,
                  std::move(mInsertComponents.front()),
                  false);

    // create new rows or reuse cached ones
    const uint new_rows_count = mInsertLinesCount - 1;
    std::vector<Row*> new_rows;
    if(mCachedRows.size() != 0){
        assert(mCachedRows.size() == new_rows_count);
        new_rows = std::move(mCachedRows);
        mCachedRows.clear();
    }else{
        new_rows = std::vector<Row*>(new_rows_count);
        for(uint i = 0; i < new_rows_count; i++)
            new_rows[i] = new Row(std::move(mInsertComponents.at(i + 1)));
    }

    // insert the rest components into the last row being inserted
    Row* last_row = new_rows.back();
    const uint last_row_children_count = last_row->childrenCount();
    if(rest.size() > 0)
        mEditor.insert(last_row, last_row_children_count, std::move(rest), false);

    // insert new rows into insertionRow's parent and update geometry
    mInsertLinePos = static_cast<uint>(pos_in_parent);
    mEditor.insertRows(container, mInsertLinePos + 1, std::move(new_rows), true);

    mInsertComponents.clear();
    mNewCursorPos = last_row_children_count;
    mSelectionController.setCursor(last_row, mNewCursorPos);
}

void InsertMultipleAction::discard()
{
    Node* parent = mInsertionRow->getParent();
    assert(parent->getType() == Component::Type::Container);

    Container* container = static_cast<Container*>(parent);
    Row* insrtion_front_line = container->getRow(mInsertLinePos);
    Row* insertion_back_line = container->getRow(mInsertLinePos + mInsertLinesCount - 1);

    assert(insrtion_front_line == mInsertionRow);

    std::vector<Component*> rest;
    rest = mEditor.remove(insrtion_front_line, mInsertionPos,
                                                  insrtion_front_line->childrenCount(), false);

    assert(mInsertComponents.size() == 0);
    mInsertComponents.push_back(std::move(rest));

    rest = mEditor.remove(insertion_back_line, mNewCursorPos,
                          insertion_back_line->childrenCount(), false);
    mEditor.insert(mInsertionRow, mInsertionPos, std::move(rest), false);

    const uint begin_line = mInsertLinePos + 1;
    const uint end_line = begin_line + mInsertLinesCount - 1;
    mCachedRows = mEditor.removeRows(container, begin_line, end_line, true);

    mNewCursorPos = mInsertionPos;
    mSelectionController.setCursor(mInsertionRow, mNewCursorPos);
}

