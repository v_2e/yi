#include "InsertFlatAction.h"


InsertFlatAction::InsertFlatAction(ComponentEditor &editor,
                                   SelectionController &selectionController,
                                   Row *where,
                                   const uint &at,
                                   std::vector<Component *> &&what):
    editor(editor),
    selectionController(selectionController),
    row(where),
    pos(at),
    insertComponents(std::move(what))
{
}

InsertFlatAction::~InsertFlatAction()
{
    for(auto c : insertComponents)
        delete c;
}

void InsertFlatAction::apply()
{
    const uint n = static_cast<uint>(insertComponents.size());
    editor.insert(row, pos, std::move(insertComponents), true);
    insertComponents.clear();
    newCursorPos = pos + n;
    selectionController.setCursor(row, newCursorPos);
}

void InsertFlatAction::discard()
{
    insertComponents = editor.remove(row, pos, newCursorPos, true);
    newCursorPos = pos;
    selectionController.setCursor(row, newCursorPos);
}
