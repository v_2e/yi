#ifndef REMOVESINGLEACTION_H
#define REMOVESINGLEACTION_H

#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Container.h"

class RemoveSingleAction : public Action
{
public:
    RemoveSingleAction(ComponentEditor &editor,
                       SelectionController &selectionController,
                       Row* where, const uint& at);
    ~RemoveSingleAction();

    void apply();
    void discard();

private:
    ComponentEditor &editor;
    SelectionController &selectionController;
    Row* removalRow;
    uint removePos;
    Component* removedComponent = nullptr;
};

#endif // REMOVESINGLEACTION_H
