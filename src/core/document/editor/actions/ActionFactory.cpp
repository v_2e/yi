#include "ActionFactory.h"

#include "InsertSingleAction.h"
#include "InsertFlatAction.h"
#include "InflateRowAction.h"
#include "InsertMultipleAction.h"
#include "RemoveSingleAction.h"
#include "RemoveMultipleAction.h"


ActionFactory::ActionFactory(ComponentEditor &editor,
                             SelectionController &selectionController):
    editor(editor),
    selectionController(selectionController)
{

}

Action *ActionFactory::createInsertAction(Row *where, const uint &pos, Component *what)
{
    return new InsertSingleAction(editor, selectionController, where, pos, what);
}

Action *ActionFactory::createInsertAction(Row *where, const uint &pos, std::vector<std::vector<Component *> > &&components)
{
    assert(where != nullptr);

    Node* parent = where->getParent();
    assert(parent != nullptr);

    const size_t lines_count = components.size();
    const bool expandable = where->isExpandable();

    if(lines_count > 1 || !expandable){
        const bool expanded = parent->getType() == Component::Type::Container;
        if(expanded)
            return new InsertMultipleAction(editor, selectionController,
                                            where, pos, std::move(components));
        else
            return new InflateRowAction(editor, selectionController,
                                        where, pos, std::move(components));
    }else{

        size_t n = 0;
        for(auto l : components)
            n += l.size();
        std::vector<Component*> flatten(n);
        n = 0;
        for(auto l : components)
            for(auto c : l)
                flatten[n++] = c;

        return new InsertFlatAction(editor, selectionController,
                                    where, pos, std::move(flatten));
    }

}

Action *ActionFactory::createRemoveAction(Row *where,const uint &at)
{
    return new RemoveSingleAction(editor, selectionController, where, at);
}


Action *ActionFactory::createRemoveAction(Container *where,
                                                  const uint &begin_line,
                                                  const uint &begin_pos,
                                                  const uint &end_line,
                                                  const uint &end_pos)
{
    return new RemoveMultipleAction(editor, selectionController,
                                    where,
                                    begin_line, begin_pos,
                                    end_line, end_pos);
}
