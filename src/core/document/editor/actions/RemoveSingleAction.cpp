#include "RemoveSingleAction.h"


RemoveSingleAction::RemoveSingleAction(ComponentEditor &editor,
                                       SelectionController &selectionController,
                                       Row *where,
                                       const uint &at):
    editor(editor),
    selectionController(selectionController),
    removalRow(where),
    removePos(at)
{

}

RemoveSingleAction::~RemoveSingleAction()
{
    if(removedComponent != nullptr)
        delete removedComponent;
}

void RemoveSingleAction::apply()
{
    removedComponent = editor.remove(removalRow, removePos);
    selectionController.setCursor(removalRow, removePos);
}

void RemoveSingleAction::discard()
{
    editor.insert(removalRow, removePos, removedComponent);
    removedComponent = nullptr;
    selectionController.setCursor(removalRow, removePos + 1);
}
