#ifndef INSERTSINGLEACTION_H
#define INSERTSINGLEACTION_H


#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Container.h"

class InsertSingleAction : public Action
{

public:
    InsertSingleAction(ComponentEditor &editor,
                       SelectionController &selectionController,
                       Row* where,
                       const uint& pos,
                       Component* what);    
    ~InsertSingleAction();

    void apply();
    void discard();

private:
    ComponentEditor &editor;
    SelectionController &selectionController;
    Row* insertRow;
    uint insertPos;
    Component* insertComponent;

};

#endif // INSERTSINGLEACTION_H
