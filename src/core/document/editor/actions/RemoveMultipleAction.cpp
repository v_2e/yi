#include "RemoveMultipleAction.h"


RemoveMultipleAction::RemoveMultipleAction(ComponentEditor &editor,
                                           SelectionController &selectionController,
                                           Container *where,
                                           const uint &begin_line, const uint &begin_pos,
                                           const uint &end_line, const uint &end_pos):
    editor(editor),
    selectionController(selectionController),
    removalContainer(where),
    begin_line(begin_line),
    begin_pos(begin_pos),
    end_line(end_line),
    end_pos(end_pos)
{

}

RemoveMultipleAction::~RemoveMultipleAction()
{
    for(auto line : removedComponents)
        for(auto component : line)
            delete component;
}

void RemoveMultipleAction::apply()
{
//    removedComponents = editor.remove(removalContainer, begin_line, begin_pos, end_line, end_pos);
//    selectionController.setCursor(removalContainer, begin_line, begin_pos);
}

void RemoveMultipleAction::discard()
{
//    editor.insert(removalContainer, begin_line, begin_pos, std::move(removedComponents));
//    removedComponents.clear();
//    selectionController.setSelection(removalContainer, begin_line, begin_pos, end_line, end_pos);
//    selectionController.setCursor(removalContainer, end_line, end_pos);
}
