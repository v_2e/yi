#ifndef REMOVEMULTIPLEACTION_H
#define REMOVEMULTIPLEACTION_H

#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Container.h"

class RemoveMultipleAction : public Action
{
public:
    RemoveMultipleAction(ComponentEditor &editor,
                       SelectionController &selectionController,
                       Container* where,
                       const uint& begin_line, const uint& begin_pos,
                       const uint& end_line, const uint& end_pos);
    ~RemoveMultipleAction();

    void apply();
    void discard();

private:
    ComponentEditor &editor;
    SelectionController &selectionController;
    Container* removalContainer;
    uint begin_line, begin_pos;
    uint end_line, end_pos;
    std::vector<std::vector<Component*>> removedComponents;


//    ComponentEditor &mEditor;
//    SelectionController &mSelectionController;
//    Row* mInsertionRow;
//    uint mInsertionPos;
//    uint mNewCursorPos;
//    std::vector<std::vector<Component*>> mInsertComponents;

//    uint mInsertLinePos;
//    uint mInsertLinesCount = 0;
//    std::vector<Row*> mCachedRows;
};

#endif // REMOVEMULTIPLEACTION_H
