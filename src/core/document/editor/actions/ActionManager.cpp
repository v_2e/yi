#include "ActionManager.h"


ActionManager::ActionManager() : QObject(nullptr)
{

}

void ActionManager::execute(Action *action)
{
    assert(action != nullptr);

    if(mRedoStack.size() > 0)
        clear(mRedoStack);

    action->apply();

    trim(mUndoStack);
    mUndoStack.push_back(action);

    emit executed();
}

void ActionManager::setUndoStackSize(const unsigned long &size)
{
    if(size != mUndoStackSize){
        mUndoStackSize = size;
        trim(mUndoStack);
        trim(mRedoStack);
    }
}

void ActionManager::undo()
{
    if(canUndo()){
        Action* action = mUndoStack.back();
        mUndoStack.pop_back();

        action->discard();

        trim(mRedoStack);
        mRedoStack.push_back(action);

        emit executed();
    }
}

void ActionManager::redo()
{
    if(canRedo()){
        Action* action = mRedoStack.back();
        mRedoStack.pop_back();

        action->apply();

        trim(mUndoStack);
        mUndoStack.push_back(action);

        emit executed();
    }
}

void ActionManager::clear(std::deque<Action *> &deque)
{
    for(auto a : deque)
        clearAction(a);
    deque.clear();
}

void ActionManager::trim(std::deque<Action *> &deque)
{
    const unsigned long int max_size = std::max(mUndoStackSize, deque.size());
    while(deque.size() >= max_size){
        clearAction(deque.front());
        deque.pop_front();
    }
}

void ActionManager::clearAction(Action *action)
{
    delete action;
}

