#ifndef ACTION_H
#define ACTION_H


class Action
{

public:
    explicit Action();
    virtual ~Action() = 0;

    virtual void apply() = 0;
    virtual void discard() = 0;

};

#endif // ACTION_H
