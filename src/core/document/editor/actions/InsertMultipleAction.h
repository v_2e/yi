#ifndef INSERTMULTIPLEACTION_H
#define INSERTMULTIPLEACTION_H

#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Row.h"
#include "Container.h"


class InsertMultipleAction : public Action
{

public:
    InsertMultipleAction(ComponentEditor &mEditor,
                         SelectionController &mSelectionController,
                         Row* where, const uint& at,
                         std::vector<std::vector<Component*>> &&what);
    ~InsertMultipleAction();

    void apply();
    void discard();

private:
    ComponentEditor &mEditor;
    SelectionController &mSelectionController;
    Row* mInsertionRow;
    uint mInsertionPos;
    uint mNewCursorPos;
    std::vector<std::vector<Component*>> mInsertComponents;

    uint mInsertLinePos;
    uint mInsertLinesCount = 0;
    std::vector<Row*> mCachedRows;

};

#endif // INSERTMULTIPLEACTION_H
