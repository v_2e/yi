#include "InsertSingleAction.h"

InsertSingleAction::InsertSingleAction(ComponentEditor &editor,
                                       SelectionController &selectionController,
                                       Row *where,
                                       const uint &pos,
                                       Component *what):
    editor(editor),
    selectionController(selectionController),
    insertRow(where),
    insertPos(pos),
    insertComponent(what)
{
}

InsertSingleAction::~InsertSingleAction()
{
    if(insertComponent != nullptr)
        delete insertComponent;
}

void InsertSingleAction::apply()
{
    editor.insert(insertRow, insertPos, insertComponent);
    insertComponent = nullptr;
    selectionController.setCursor(insertRow, insertPos + 1);
}

void InsertSingleAction::discard()
{
    insertComponent = editor.remove(insertRow, insertPos);
    selectionController.setCursor(insertRow, insertPos);
}
