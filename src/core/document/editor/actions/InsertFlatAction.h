#ifndef INSERTFLATACTION_H
#define INSERTFLATACTION_H


#include "Action.h"
#include "ComponentEditor.h"
#include "SelectionController.h"
#include "Component.h"
#include "Row.h"


class InsertFlatAction : public Action
        {

public:
    InsertFlatAction(ComponentEditor &editor,
                     SelectionController &selectionController,
                     Row* where, const uint& at,
                     std::vector<Component*> &&what);
    ~InsertFlatAction();

    void apply();
    void discard();


private:
    ComponentEditor &editor;
    SelectionController &selectionController;
    Row* row;
    uint pos;
    uint newCursorPos;
    std::vector<Component*> insertComponents;

};

#endif // INSERTFLATACTION_H
