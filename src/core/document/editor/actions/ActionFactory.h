#ifndef ACTIONFACTORY_H
#define ACTIONFACTORY_H

#include "ComponentEditor.h"
#include "SelectionController.h"

class Action;

class ActionFactory
{
public:
    ActionFactory(ComponentEditor& editor, SelectionController& selectionController);

    Action* createInsertAction(Row* where,
                               const uint& pos,
                               Component* what);

    Action* createInsertAction(Row* where,
                               const uint& pos,
                               std::vector<std::vector<Component*>> &&components);

    Action* createRemoveAction(Row* where, const uint& at);

    Action* createRemoveAction(Container* where,
                               const uint& begin_line, const uint& begin_pos,
                               const uint& end_line, const uint& end_pos);

private:
   ComponentEditor& editor;
   SelectionController& selectionController;

};

#endif // ACTIONFACTORY_H
