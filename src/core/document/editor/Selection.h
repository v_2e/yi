#ifndef SELECTION_H
#define SELECTION_H

#include <QObject>


class Container;
typedef unsigned int uint;

class Selection : public QObject
{

    Q_OBJECT

    friend class SelectionController;

public:
    Selection();
    ~Selection();

    inline Container* holder() const { return mHolder; }
    inline uint beginLine() const { return mBeginLine; }
    inline uint beginPos() const { return mBeginPos; }
    inline uint endLine() const { return mEndLine; }
    inline uint endPos() const { return mEndPos; }
    inline bool reverse() const { return mReverse; }

signals:
    void changed(const Selection& selection);


private:
    Container* mHolder = nullptr;
    uint mBeginLine = 0;
    uint mBeginPos = 0;
    uint mEndLine = 0;
    uint mEndPos = 0;
    bool mReverse = false;

};

#endif // SELECTION_H
