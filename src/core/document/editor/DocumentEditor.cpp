#include "DocumentEditor.h"
#include "Document.h"
#include "RootNode.h"

DocumentEditor::DocumentEditor(Document *document) :
    QObject (nullptr),
    mDocument(document),
    mSelectionController(mSelection, mCursor),
    mActionFactory(mComponentEditor, mSelectionController)
{
    mSelectionController.setCursor(mDocument->root().getRow(0),  0);

    connect(&mActionManager, &ActionManager::executed, this, &DocumentEditor::documentChanged);
}

DocumentEditor::~DocumentEditor()
{
    disconnect(&mActionManager, &ActionManager::executed, this, &DocumentEditor::documentChanged);
}

void DocumentEditor::setCursor(Row *row, const uint & pos)
{
    mSelectionController.setCursor(row, pos);
}

void DocumentEditor::setSelection(Container *container,
                                  const uint & begin_line, const uint & begin_pos,
                                  const uint & end_line, const uint & end_pos,
                                  const bool& reverse)
{
    mSelectionController.setSelection(container, begin_line, begin_pos, end_line, end_pos, reverse);
}

void DocumentEditor::clearSelection()
{
    mSelectionController.setSelection(nullptr, 0, 0, 0, 0);
}

void DocumentEditor::insert(Row *where,
                            const uint &pos,
                            Component *component)
{
    Action* insertAction = mActionFactory
            .createInsertAction(where, pos, component);
    mActionManager.execute(insertAction);
}

void DocumentEditor::insert(Row *where,
                            const uint &pos,
                            std::vector<std::vector<Component*>> &&components)
{
    Action* insertAction = mActionFactory
            .createInsertAction(where, pos, std::move(components));
    mActionManager.execute(insertAction);
}

void DocumentEditor::remove(Row *where, const uint &at)
{

    Action* removeAction = mActionFactory
            .createRemoveAction(where, at);
    mActionManager.execute(removeAction);
}

void DocumentEditor::remove(Container *container,
                            const uint &begin_line, const uint &begin_pos,
                            const uint &end_line, const uint &end_pos)
{
    Action* removeAction = mActionFactory
            .createRemoveAction(container, begin_line, begin_pos, end_line, end_pos);
    mActionManager.execute(removeAction);
}

void DocumentEditor::setUndoStackSize(const unsigned long &size)
{
    mActionManager.setUndoStackSize(size);
}

void DocumentEditor::undo()
{
    if(mActionManager.canUndo())
        mActionManager.undo();
}

void DocumentEditor::redo()
{
    if(mActionManager.canRedo())
        mActionManager.redo();
}

void DocumentEditor::notifyDocumentChanged()
{
    emit documentChanged();
}

bool DocumentEditor::canUndo() const
{
    return mActionManager.canUndo();
}

bool DocumentEditor::canRedo() const
{
    return mActionManager.canRedo();
}


