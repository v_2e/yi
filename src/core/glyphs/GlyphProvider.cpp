#include "GlyphProvider.h"
#include <QDebug>

#include "GlyphDrawable.h"

static QRawFont* font;

GlyphProvider::GlyphProvider()
{
    QString fonts[] = {
        ":/fonts/computer_modern/cmunci.ttf",
        ":/fonts/STIX2Math.otf",
        ":/fonts/computer_modern/cmunti.ttf",    /* Latin Italic */
        ":/fonts/STIX2Text-Italic.otf",
        ":/fonts/latinmodern-math.otf",             /* + -  < > Operators \int */
        ":/fonts/euclid/Euclid Extra Regular.ttf",  /* Spec symbols, fences */
        ":/fonts/euclid/Euclid Math One Regular.ttf",  /* Math script? */
        ":/fonts/euclid/Euclid Math Two Regular.ttf",
        ":/fonts/euclid/Euclid Symbol Italic.ttf",
        ":/fonts/euclid/Euclid Symbol Regular.ttf",
        ":/fonts/dsserif/DSSerif.pfb",
        ":/fonts/tex-gyre-math/texgyrebonum-math.otf",
        ":/fonts/tex-gyre-math/texgyredejavu-math.otf",
        ":/fonts/tex-gyre-math/texgyrepagella-math.otf",
        ":/fonts/tex-gyre-math/texgyreschola-math.otf",
        ":/fonts/tex-gyre-math/texgyretermes-math.otf",
    };

    for(auto fname : fonts)
        QFontDatabase::addApplicationFont(fname);



    const QString font_resource(fonts[4]);
    const qreal font_size = 74.0;

//    QFont f("STIX", font_size);
//    f.setItalic(true);
//    f.setBold(true);
//    font = new QRawFont(QRawFont::fromFont(f));

    font = new QRawFont();
    font->loadFromFile(font_resource, font_size, QFont::PreferDefaultHinting);

}

GlyphProvider::~GlyphProvider()
{

}

GlyphDrawableHandle *GlyphProvider::provideDrawable(const IGlyphProvider::Request &request)
{
    QString hash = QString::fromUcs4(&request.glyph_code, 1);
    GlyphDrawableHandle* handle = mDrawables.value(hash);
    if(handle == nullptr){
        handle = new GlyphDrawableHandle;
        handle->id = hash;
        handle->drawable = new GlyphDrawable(request.glyph_code, font);
        mDrawables.insert(handle->id, handle);
//        qDebug() << "Create new drawable.";
    }else{
//        qDebug() << "Cached drawable found.";
    }
    incrementUseCounter(handle);
    return handle;
}

void GlyphProvider::recycleDrawable(GlyphDrawableHandle *handle)
{
    qDebug() << "Drawable recycled";
}

GlyphDrawableHandle *GlyphProvider::create(const IGlyphProvider::Request &request)
{

}

void GlyphProvider::remove(GlyphDrawableHandle *handle)
{

}
