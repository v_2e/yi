#ifndef GLYPHPROVIDER_H
#define GLYPHPROVIDER_H

#include "GlyphDrawable.h"
#include "IGlyphProvider.h"

#include <unordered_map>
#include <QMap>

class GlyphProvider : public IGlyphProvider
{

public:
    GlyphProvider();
    ~GlyphProvider();

    GlyphDrawableHandle* provideDrawable(const Request &request);
    void recycleDrawable(GlyphDrawableHandle* handle);




private:
    QHash<QString, GlyphDrawableHandle*> mDrawables;

    GlyphDrawableHandle* create(const Request& request);
    void remove(GlyphDrawableHandle* handle);

};

#endif // GLYPHDRAWABLEPROVIDERIMPL_H
