#ifndef CHARACTERTYPE_H
#define CHARACTERTYPE_H

enum class CharacterType{
    LatinLowercase,
    LatinUppercase,
    GreekLowercase,
    GreekUppercase,
    MathOperator,
    MathSymbol
};

#endif // CHARACTERTYPE_H
