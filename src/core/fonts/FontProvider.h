#ifndef FONTPROVIDER_H
#define FONTPROVIDER_H

#include <string>

class QRawFont;

struct FontHandle{
    std::string *font_id;
    QRawFont *font_data;
};

struct FontRequest{
    std::string family;
    int size = 12;
    bool italic = false;
    bool bold = false;
};

class FontProvider
{
public:
    FontProvider();
    virtual ~FontProvider() = 0;
    virtual FontHandle getDefaultFont() = 0;
    virtual FontHandle getFont(const FontRequest& request) = 0;
    virtual FontHandle recycleFont(FontHandle& font) = 0;
};

#endif // FONTPROVIDER_H
