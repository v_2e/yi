#ifndef FONTLOADER_H
#define FONTLOADER_H

class QFontDatabase;
class QRawFont;
class QString;

class FontLoader
{
public:
    FontLoader(QFontDatabase* database);
    QRawFont* loadFont(const QString &family, const int &size);

private:
    QFontDatabase* mFontDatabase;

};

#endif // FONTLOADER_H
