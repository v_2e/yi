#include "FontsManager.h"

#include <QFontDatabase>
#include <QRawFont>

FontsManager::FontsManager() : QObject (nullptr),
    mLoader(&mFontDatabase)
{
    initFontsDatabase();
}

FontsManager::~FontsManager()
{

}

QStringList FontsManager::availableFamilies()
{
    return mFontDatabase.families();
}

FontHandle FontsManager::getFont(const std::string &family, const int &size)
{
    std::string font_id = family + " " + std::to_string(size);

    // check if font is already loaded


    // load font

    return getDefaultFont();
}

FontHandle FontsManager::getDefaultFont()
{
    return {nullptr, nullptr};
}

FontHandle FontsManager::recycleFont(FontHandle &font)
{
}

void FontsManager::initFontsDatabase()
{

}

FontHandle *FontsManager::createFont(const std::string &name, const int &size)
{

}

