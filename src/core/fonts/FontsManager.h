#ifndef FONTSMANAGER_H
#define FONTSMANAGER_H

#include <QObject>
#include <QFontDatabase>
#include <unordered_map>

#include "FontProvider.h"
#include "FontLoader.h"



class FontsManager : public QObject, FontProvider
{

    Q_OBJECT

public:
    FontsManager();
    ~FontsManager();

    QStringList availableFamilies();


    FontHandle getFont(const std::string& name, const int& size);
    FontHandle getDefaultFont();
    FontHandle recycleFont(FontHandle& font);

//    bool isFontAvailable(const std::string& family) const;

//public:
//    bool isFontLoaded(const std::string& family, const int& size) const;
//    Font& loadFont(const std::string& family, const int& size);

private:
    typedef unsigned int counter;
    typedef std::pair<FontHandle*, counter> font_entry;
    std::unordered_map<std::string, font_entry> mFontsPool;

private:
    QFontDatabase mFontDatabase;
    FontLoader mLoader;
    void initFontsDatabase();
    FontHandle* createFont(const std::string& name, const int& size);

};

#endif // FONTSMANAGER_H
