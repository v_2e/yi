 QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = yi
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

DESTDIR = ../build
MOC_DIR = ../build
UI_HEADERS_DIR = ../build
UI_SOURCES_DIR = ../build
UI_DIR = ../build
OBJECTS_DIR = ../build

INCLUDEPATH += window/main                       \
               window/sandbox                    \
               core/utils                        \
               core/streams                      \
               core/document                     \
               core/document/view                \
               core/document/view/drawable       \
               core/document/structure           \
               core/document/structure/base      \
               core/document/structure/operations\
               core/document/factory             \
               core/document/editor              \
               core/document/editor/actions      \
               core/document/editor/events       \
               core/fonts                        \
               core/glyphs                       \
               view                              \
               view/math                         \
               view/math/internal/base           \
               view/math/internal/utils          \
               view/math/internal                \
               view/worksheet                    \
               view/menu                         \
               controller/worksheet              \
               controller/menu                   \
               utils                             \
               utils/test/viewbuilder            \
               utils/test/pool

HEADERS += $$files(*.h, true)
SOURCES += $$files(*.cpp, true)
FORMS   += $$files(*.ui, true)
RESOURCES += $$files(*.qrc, true)

QMAKE_CXXFLAGS += -std=gnu++17
QMAKE_RPATHDIR += .

