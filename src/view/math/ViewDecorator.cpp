#include "ViewDecorator.h"

#include "Row.h"
#include "Container.h"

#include "Cursor.h"
#include "Selection.h"

#include <QPainter>


ViewDecorator::ViewDecorator()
{

}

ViewDecorator::~ViewDecorator()
{

}

void ViewDecorator::clear()
{
//    setCursorFocus(nullptr);
//    setSelectionHolder(nullptr);
}

void ViewDecorator::setCursor(const Cursor &cursor)
{
//    setCursorFocus(cursor.focus());
    cursorPos = cursor.pos();
}

void ViewDecorator::setSelection(const Selection &selection)
{
//    setSelectionHolder(selection.holder());
}

void ViewDecorator::drawCursor(Row *row)
{
    QPainter p(row);
    p.setBrush(Qt::darkCyan);
    if(row->childrenCount() > 0 && cursorPos > 0){
        if(cursorPos < row->childrenCount())
            p.drawRect(row->getChild(cursorPos)->x() - 1, 0, 2, row->height());
        else
            p.drawRect(row->width() - 4, 0, 2, row->height());
    }else{
        p.drawRect(4, 0, 2, row->height());
    }
    p.end();
}

void ViewDecorator::drawSelection(Container *container)
{
    Q_UNUSED(container)
}
