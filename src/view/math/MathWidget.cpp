#include "MathWidget.h"

#include <QDebug>

#include "CursorDrawer.h"
#include <QScrollBar>

MathWidget::MathWidget(QWidget *parent) :
  ScrollArea(parent)
{
    setPalette(Qt::white);

    mCursorMapper = new CursorMapper;
    mDocumentLayer = new DocumentLayer(this);
    setWidget(mDocumentLayer);

    mCursorDrawer = new CursorDrawer(mDocumentLayer->overlay());
    mDocumentLayer->overlay()->addDrawer(mCursorDrawer);

}


MathWidget::~MathWidget()
{
    delete mDocumentLayer;
    delete mCursorMapper;
}

void MathWidget::setDocument(Document *document)
{
    if(document != nullptr){
        if(mDocument != nullptr)
            detachDocument();
        attachDocument(document);
    }else{
        detachDocument();
    }
}

void MathWidget::attachDocument(Document *document)
{

    mDocument = document;
    mDocument->attach(this);

    connect(&mDocument->editor(), &DocumentEditor::documentChanged, this, [this](){
       mCursorDrawer->setCursor(mDocument->editor().cursor());
       mDocumentLayer->adjustSize();
    });

    mDocumentLayer->attachDocument(&mDocument->root());
    mDocumentLayer->setVisible(true);

    mCursorDrawer->setCursor(mDocument->editor().cursor());
    mCursorDrawer->setEnabled(true);
    connect(&mDocument->editor().cursor(), &Cursor::changed, this, &MathWidget::onCursorFocusChanged);


}

void MathWidget::detachDocument()
{
    mCursorDrawer->setEnabled(false);
    disconnect(&mDocument->editor().cursor(), &Cursor::changed, this, &MathWidget::onCursorFocusChanged);


    mDocument->root().hide();
    mDocument->detach();
    mDocument = nullptr;
    mDocumentLayer->detachDocument();
    mDocumentLayer->setVisible(false);
}

void MathWidget::onCursorFocusChanged(const Cursor &cursor)
{
    mCursorDrawer->setCursor(cursor);
}


// Document structure events
void MathWidget::onKeyPress(QKeyEvent *ke)
{
    emit keyPressed(ke);
}

void MathWidget::onMousePress(Component *component, QMouseEvent *event)
{
    Q_UNUSED(component);
    if(event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier)){
        this->mIsScrolling = true;
        this->mMousePoint = event->globalPos();
        return;
    }

    QPoint p = event->pos();
    qDebug() << "Mouse clicked: " << event->pos();
    switch (component->getType()) {
    case Component::Type::Container:{
        Container* container = static_cast<Container*>(component);
        if(container->rowsCount() > 0){
            uint i_row = mCursorMapper->findCursorLine(container, p);
            qDebug() << "Row index: " << i_row;
            Row* row = container->getRow(i_row);
            uint pos = mCursorMapper->findCursorPosition(row, p);
            mDocument->editor().setCursor(row, pos);
        }
    }   break;
    case Component::Type::Row:{
        Row* row = static_cast<Row*>(component);
        uint pos = mCursorMapper->findCursorPosition(row, p);
        mDocument->editor().setCursor(row, pos);
        break;
    }
    default:
        break;
    }
}

void MathWidget::onMouseMove(Component *component, QMouseEvent *event)
{
    Q_UNUSED(component)
    Q_UNUSED(event)
    if(mIsScrolling && event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier)){
        QPoint d = event->globalPos() - this->mMousePoint;
        scroll(- d.x(), - d.y());
        this->mMousePoint = event->globalPos();
        return;
    }
}

void MathWidget::onMouseRelease(Component *component, QMouseEvent *event)
{
    Q_UNUSED(component);
    Q_UNUSED(event);
    mIsScrolling = false;
}

void MathWidget::wheelEvent(QWheelEvent *we)
{
    if(we->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier)){
        QPoint mousePoint = we->globalPos() - mDocumentLayer->mapToGlobal(QPoint(0, 0));
        const float ds = 0.1f;
        const float s = we->angleDelta().y() > 0 ? 1.0f + ds : 1.0f - ds;
        scale(s, mousePoint);
    }else{
        ScrollArea::wheelEvent(we);
    }
}

void MathWidget::scale(float scale, const QPoint &center)
{
    const QPointF dt = center * (scale - 1.0f);
    bool scaled = false;
    if(mDocument != nullptr){
        RootNode* root = &mDocument->root();
        const int newScalePrc = qRound(scale * root->getBaseScale());
        scaled = mDocument->root().setBaseScale(newScalePrc);
        mDocumentLayer->adjustSize();
        mCursorDrawer->setCursor(mDocument->editor().cursor());
    }
    if(scaled){
        scroll(qRound(dt.x()), qRound(dt.y()));
        qDebug() << "Translate by: " << dt;
    }
}

// MathWidget events
void MathWidget::mousePressEvent(QMouseEvent *me)
{
    if(mDocument != nullptr){
        RootNode* root = &mDocument->root();
        me->setLocalPos(me->pos() - root->pos());
        onMousePress(root, me);
    }
}

void MathWidget::mouseMoveEvent(QMouseEvent *me)
{
    if(mDocument != nullptr){
        Node* root = &mDocument->root();
        QPoint inRootPos = me->pos() - root->pos();
        me->setLocalPos(inRootPos);
        onMouseMove(root, me);
    }
}

void MathWidget::mouseReleaseEvent(QMouseEvent *me)
{
    if(mDocument != nullptr){
        QPoint inRootPos = me->pos() - mDocument->root().pos();
        me->setLocalPos(inRootPos);
        onMouseRelease(&mDocument->root(), me);
    }
}


