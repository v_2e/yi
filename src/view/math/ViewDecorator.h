#ifndef VIEWDECORATOR_H
#define VIEWDECORATOR_H


class Cursor;
class Selection;

class Row;
class Container;

typedef unsigned int uint;

class Decorator{

};

class ViewDecorator : public Decorator
{
public:
    ViewDecorator();
    ~ViewDecorator();

    void clear();

    void setCursor(const Cursor& cursor);
    void setSelection(const Selection& selection);

    inline void drawCursor(Row* container);
    inline void drawSelection(Container* container);

private:
    uint cursorPos = 0;

};

#endif // VIEWDECORATOR_H
