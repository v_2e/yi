#ifndef MATHWIDGET_H
#define MATHWIDGET_H

#include "ScrollArea.h"
#include "Layer.h"
#include "DocumentLayer.h"

#include "Document.h"
#include "Context.h"
#include "Cursor.h"
#include "Selection.h"
#include "CursorMapper.h"

class CursorDrawer;

class MathWidget : public ScrollArea, public Context
{

    Q_OBJECT

public:
    explicit MathWidget(QWidget *parent = nullptr);
    ~MathWidget();
    void setDocument(Document *document);

    // MathWidget own events
    void mousePressEvent(QMouseEvent *me);
    void mouseMoveEvent(QMouseEvent *me);
    void mouseReleaseEvent(QMouseEvent *me);

    // Document view events
    void onKeyPress (QKeyEvent* ke) final;
    void onMousePress (Component* component,  QMouseEvent* event) final;
    void onMouseMove(Component* component,  QMouseEvent* event) final;
    void onMouseRelease(Component* component,  QMouseEvent* event) final;
    void wheelEvent(QWheelEvent *we);
    void scale(float scale, const QPoint &center);


private slots:
    void onCursorFocusChanged(const Cursor &cursor);

signals:
    void keyPressed(QKeyEvent *ke);


private:
    Document* mDocument = nullptr;
    void attachDocument(Document* document);
    void detachDocument();


private:
    DocumentLayer* mDocumentLayer;
    CursorDrawer *mCursorDrawer;
    CursorMapper *mCursorMapper;

    bool mIsScrolling;
    QPoint mMousePoint;




};

#endif // MATHWIDGET_H
