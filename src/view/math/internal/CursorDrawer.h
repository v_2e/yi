#ifndef CURSORDRAWER_H
#define CURSORDRAWER_H

#include "Drawer.h"
#include <QPainter>
#include <QPen>
#include <QTimer>

#include "Cursor.h"
#include "Row.h"

#include <QFont>
#include <QRawFont>


class CursorDrawer : public Drawer
{

public:
    CursorDrawer(Layer *layer);
    void setCursor(const Cursor& cursor);


    void setEnabled(bool enabled);
    void setViewport(const QRect& viewport);

    void setWidth(int width);
    void setColor(QColor color);
    void setBlinkRate(uint ms);

    void draw(Layer *layer);

private:
    void startBlinking();
    void updateCursorRect();

private:
    QTimer mBlinkTimer;
    int mBlinkRate = 600;
    bool mIsFade = false;

    QRect mViewportRect;
    QRect mCursorRect;
    int mCursorWidth = 2;

    QPainter mPainter;
    QPen mPen;

};

#endif // CURSORDRAWER_H
