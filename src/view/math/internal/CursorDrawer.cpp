#include "CursorDrawer.h"
#include "Layer.h"

#include <QRect>

#include "debug.h"


CursorDrawer::CursorDrawer(Layer *layer) :
    Drawer(layer)
{
    mPen.setColor(Qt::black);
    mPen.setWidth(mCursorWidth);

    connect(&mBlinkTimer, &QTimer::timeout, this, [this](){
        mIsFade = !mIsFade;
        this->requestUpdate(mCursorRect);
    });

    setEnabled(false);
}

void CursorDrawer::setCursor(const Cursor &cursor)
{
    Row* row = cursor.focus();
    const uint pos = cursor.pos();
    const uint children_count = row->childrenCount();

    const int cursor_offset = 1;
    int cursor_x;

    if(children_count > 0){
        if(pos > 0){
            if(pos < children_count){
//                cursor_x = row->getChild(pos - 1)->right();
                Component* c = row->getComponent(pos - 1);
                cursor_x = c->x() + (c->width() + c->horizontalAdvance())/2;
            }else{
                cursor_x = row->width() - cursor_offset;
            }
        }else{
             cursor_x = cursor_offset;
        }
    }else{
        cursor_x = row->width() - cursor_offset;
    }
    int cursor_height_padding = 3;
    int rect_height = row->height() + cursor_height_padding*2;
    int rect_width = mCursorWidth + 2*cursor_offset;


    // clear previous cursor
    requestUpdate(mCursorRect);

    mCursorRect.setRect(cursor_x, -cursor_height_padding, rect_width, rect_height);
    mCursorRect.translate(row->globalPos());

    // update new cursor pos
    requestUpdate(mCursorRect);
    startBlinking();
}

void CursorDrawer::setViewport(const QRect &viewport)
{
    mViewportRect = viewport;
}

void CursorDrawer::setWidth(int width)
{
    mPen.setWidth(width);
}

void CursorDrawer::setColor(QColor color)
{
    mPen.setColor(color);
}

void CursorDrawer::setBlinkRate(uint ms)
{
    mBlinkRate = static_cast<int>(ms);
    if(isEnabled()) startBlinking();
}

void CursorDrawer::draw(Layer *layer)
{
    mPainter.begin(layer);
//    mPainter.setRenderHint(QPainter::Antialiasing);


    mPen.setWidth(mCursorWidth);
    mPen.setColor(mIsFade ? Qt::black : Qt::white);
//    mPen.setColor(mIsFade ? Qt::white : Qt::black);
    mPainter.setPen(mPen);

    const int x = mCursorRect.center().x();
    mPainter.setCompositionMode(QPainter::CompositionMode_Difference);
    mPainter.drawLine(x, mCursorRect.top() + 1, x, mCursorRect.bottom() - 1);



//    mPainter.setBrush(mIsFade ? Qt::white : Qt::black);
////    mPainter.setCompositionMode(QPainter::CompositionMode_Difference);

//    const int x = mCursorRect.center().x();
//    mPainter.drawLine(QLineF(x, mCursorRect.top() + mCursorWidth, x, mCursorRect.bottom() - mCursorWidth));

    mPainter.end();
//    qDebug() << "Draw cursor: " << mCursorRect;
}

void CursorDrawer::startBlinking()
{
    mIsFade = false;
    requestUpdate(mCursorRect);
    mBlinkTimer.stop();
    mBlinkTimer.start(mBlinkRate);
}

void CursorDrawer::updateCursorRect()
{

}

void CursorDrawer::setEnabled(bool enabled)
{
    Drawer::setEnabled(enabled);
    if(enabled) startBlinking();
}




