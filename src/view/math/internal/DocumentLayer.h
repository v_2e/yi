#ifndef DOCUMENTLAYER_H
#define DOCUMENTLAYER_H

#include "Layer.h"
#include "RootNode.h"

class DocumentLayer : public Layer
{

public:
    explicit DocumentLayer(QWidget *parent = nullptr);
    ~DocumentLayer();

    void attachDocument(RootNode* root);
    void detachDocument();
    Layer* overlay();
    void adjustSize();


    bool eventFilter(QObject *o, QEvent *e);
    void paintEvent(QPaintEvent *pe);

private:
    RootNode* mRoot;
    Layer* mOverlay;

private:
    void init();

};

#endif // DOCUMENTLAYER_H
