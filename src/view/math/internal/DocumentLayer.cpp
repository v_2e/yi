#include "DocumentLayer.h"

#include <QDebug>
#include <QResizeEvent>

DocumentLayer::DocumentLayer(QWidget *parent) : Layer(parent)
{
    init();

    QCursor cursor;
    cursor.setShape(Qt::CursorShape::IBeamCursor);
    setCursor(cursor);

    installEventFilter(this);
}

void DocumentLayer::init()
{
    setPalette(Qt::white);

    mOverlay = new Layer(this);
    mOverlay->setPalette(Qt::transparent);
    mOverlay->setAttribute(Qt::WA_TransparentForMouseEvents);
    mOverlay->move(0, 0);
    mOverlay->show();
}

DocumentLayer::~DocumentLayer()
{
    if(mRoot != nullptr)
        detachDocument();
    delete mOverlay;
}

void DocumentLayer::attachDocument(RootNode *root)
{
    mRoot = root;
    QWidget *w = root;
    w->setParent(this);
    w->move(0, 0);
    w->stackUnder(mOverlay);
    w->show();
    adjustSize();
}

void DocumentLayer::detachDocument()
{
    mRoot->hide();
//    disconnect(mDocument, &View::resized, this, &DocumentLayer::onDocumentSizeChanged);

    QWidget *w = mRoot;
    w->setParent(nullptr);
    mRoot = nullptr;
    adjustSize();
}

Layer *DocumentLayer::overlay()
{
    return mOverlay;
}

bool DocumentLayer::eventFilter(QObject *o, QEvent *e)
{
//    qDebug() << "Event: " << e->type();
    if(e->type() == QEvent::Resize){
        e->ignore();
//        qDebug() << "Filtered out : " << e->type();
        return true;
    }
    return false;
}

void DocumentLayer::paintEvent(QPaintEvent *pe)
{
    QWidget::paintEvent(pe);

//    const int d = 500;
    QPainter p(this);
//    p.setPen(QPen(Qt::blue, 1));
//    p.drawLine(0, d, width(), d);
//    int x = d;
//    while(x < width()){
//        p.drawText(QPoint(x, d + 20), QString("%1").arg(x));
//        x += 100;
//    }
//    p.drawLine(d, 0, d, height());
//    int y = d;
//    while(y < height()){
//        p.drawText(QPoint(d + 20, y), QString("%1").arg(y));
//        y += 100;
//    }
}

void DocumentLayer::adjustSize()
{
    if(mRoot != nullptr){
        QSize s = mRoot->size();
        if(size() != s){
            setFixedSize(s);
            mOverlay->setFixedSize(s);
        }
    }else{
        setFixedSize(0, 0);
        mOverlay->setFixedSize(0, 0);
    }
}

