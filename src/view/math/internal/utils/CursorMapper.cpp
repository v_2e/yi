#include "CursorMapper.h"
#include "Row.h"
#include "Container.h"
#include <QDebug>


inline uint cursor_pos_linear_search(Row* row, const int &x){
    uint pos = 0;
    const uint n = row->childrenCount();
    while(pos < n){
        if(x > row->getChild(pos)->centerX()){
            pos++;
        }else{
            return pos;
        }
    }
    return pos;
}

inline uint cursor_pos_binary_search(Row* row, const int &x){
    uint left = 0;
    uint right = row->childrenCount();
    while (left < right) {
        const uint mid = left + (right - left)/2;
        const int mid_x = row->getChild(mid)->centerX();



        if(x <= mid_x){
            right = mid;
        }else{
            if(mid != left)
                left = mid;
            else
                return right;
        }


        qDebug() << left << "(" << x << ")"<< mid <<  "(" << mid_x << ")"<< right;
    }
    return right;
}

uint CursorMapper::findCursorPosition(Row *row, const QPoint &point)
{
    const int x = point.x();
    if(x <= 0)
        return 0;
    if(x >= row->width())
        return row->childrenCount();
    return cursor_pos_binary_search(row, x);
}


inline uint cursor_row_binary_search(Container* container, const int &y){
    uint top = 0;
    uint bottom = container->rowsCount() - 1;
    while (top < bottom) {

        const uint mid = top + (bottom - top)/2;

        const int mid_y = container->getRow(mid)->bottom() +
                          (container->getRow(mid + 1)->top() - container->getRow(mid)->bottom()) / 2;
        qDebug() << top << "(" << y << ")"<< mid <<  "(" << mid_y << ")"<< right;

        if(y <= mid_y){
            bottom = mid;
        }else{
            if(mid != top)
                top = mid;
            else
                return bottom;
        }
    }
    return bottom;
}

uint CursorMapper::findCursorLine(Container *container, const QPoint &point)
{
    const uint n = container->rowsCount();
    assert(n > 0);

    const int y = point.y();
    if(y <= container->frontRow()->centerY())
        return 0;
    if(y > container->backRow()->centerY())
        return n - 1;
    return cursor_row_binary_search(container, y);
}
