#ifndef CURSORMAPPER_H
#define CURSORMAPPER_H

class Row;
class Container;
class QPoint;
typedef unsigned int uint;

class CursorMapper
{
public:
    uint findCursorPosition(Row* row, const QPoint &point);
    uint findCursorLine(Container* container, const QPoint &point);
};

#endif // ICURSORMAPPER_H
