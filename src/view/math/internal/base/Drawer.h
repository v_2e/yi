#ifndef DRAWER_H
#define DRAWER_H

#include <QObject>

class Layer;
class QRect;

class Drawer : public QObject
{

    Q_OBJECT

public:
    Drawer(Layer *layer);
    virtual ~Drawer() = 0;
    virtual void draw(Layer* layer) = 0;
    void setEnabled(bool enabled);
    inline bool isEnabled() { return mEnabled; }

protected:
    void requestUpdate(const QRect &rect);
    void updateGlobal();

private:
    Layer *mLayer;
    bool mEnabled;

};

#endif // DRAWER_H
