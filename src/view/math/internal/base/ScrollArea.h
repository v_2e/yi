#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include <QScrollArea>

class ScrollArea : public QScrollArea
{

    Q_OBJECT

public:
    explicit ScrollArea(QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *re);
    void scroll(int dx, int dy);
    virtual void viewportSizeChangeEvent(const QRect &viewport);


private:
    QRect mViewportRect;

};

#endif // DOCUMENTSCROLLER_H
