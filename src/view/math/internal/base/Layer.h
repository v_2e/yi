#ifndef LAYER_H
#define LAYER_H

#include <QWidget>

class Drawer;

class Layer : public QWidget
{

    Q_OBJECT

public:
    explicit Layer(QWidget *parent = nullptr);

    void addDrawer(Drawer *drawer);
    void removeDrawer(Drawer *drawer);

    void paintEvent(QPaintEvent* pe);

private:
    QList<Drawer*> mDrawers;

};

#endif // LAYER_H
