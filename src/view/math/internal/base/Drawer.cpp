#include "Drawer.h"

#include <Layer.h>

Drawer::Drawer(Layer *layer) :
    QObject (nullptr),
    mLayer(layer)
{

}

void Drawer::setEnabled(bool enabled) {
    mEnabled = enabled;
    if(!enabled)
        updateGlobal();
}

Drawer::~Drawer()
{

}

void Drawer::requestUpdate(const QRect &rect)
{
    mLayer->update(rect);
}

void Drawer::updateGlobal()
{
    mLayer->update();
}
