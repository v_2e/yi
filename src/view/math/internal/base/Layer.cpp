#include "Layer.h"

#include <QPaintEvent>
#include <QDebug>
#include <QElapsedTimer>

#include "Drawer.h"


Layer::Layer(QWidget *parent) : QWidget(parent)
{

}

void Layer::addDrawer(Drawer *drawer)
{
    mDrawers.append(drawer);
}

void Layer::removeDrawer(Drawer *drawer)
{
    mDrawers.removeAll(drawer);
}

void Layer::paintEvent(QPaintEvent *pe)
{
    for(Drawer* drawer : mDrawers)
        if(drawer->isEnabled())
            drawer->draw(this);
    pe->accept();
}
