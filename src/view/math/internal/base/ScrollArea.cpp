#include "ScrollArea.h"

#include <QScrollBar>
#include <QDebug>
#include <QResizeEvent>

ScrollArea::ScrollArea(QWidget *parent) : QScrollArea(parent)
{
    connect(horizontalScrollBar(), &QScrollBar::valueChanged, this, [this](int value){
        mViewportRect.moveLeft(value);
        viewportSizeChangeEvent(mViewportRect);
    });
    connect(verticalScrollBar(), &QScrollBar::valueChanged, this, [this](int value){
        mViewportRect.moveTop(value);
        viewportSizeChangeEvent(mViewportRect);
    });
}

void ScrollArea::resizeEvent(QResizeEvent *re)
{
    Q_UNUSED(re)
    mViewportRect.setSize(size());
    viewportSizeChangeEvent(mViewportRect);
}

void ScrollArea::scroll(int dx, int dy)
{
    QScrollBar *sb = horizontalScrollBar();
    sb->setValue(sb->value() + dx);
    sb = verticalScrollBar();
    sb->setValue(sb->value() + dy);
}

void ScrollArea::viewportSizeChangeEvent(const QRect &viewport)
{
    Q_UNUSED(viewport);
}

