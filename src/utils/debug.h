#ifndef DEBUG_H
#define DEBUG_H

#include "QDebug"

#define tag(A) typeid(A).name()

#endif // DEBUG_H
