#include "RandomComponentGenerator.h"
#include <random>
#include <QDebug>

//#include "DSymbol.h"
//#include "DNode.h"
//#include "DStaticNode.h"
//#include "DTableNode.h"


#define LOG_ENABLED true

//void normalize(std::map<DComponent::Type, float> &dist){
//    float total = 0.0f;
//    for(auto entry : dist){
//        total += entry.second;
//    }
//    const float rtotal = 1.0f/total;
//    for(auto &entry : dist){
//        entry.second *= rtotal;
//    }
//}

//void normalize(std::vector<float> &dist){
//    float total = 0.0f;
//    for(auto entry : dist){
//        total += entry;
//    }
//    const float rtotal = 1.0f/total;
//    for(auto &entry : dist){
//        entry *= rtotal;
//    }
//}


RandomComponentGenerator::RandomComponentGenerator()
{

//    std::random_device dev;
//    mGenerator = std::mt19937(dev());
//    mMaxDepth = 8;

//    mTypesDistribution[DComponent::Type::Symbol] = 6.0f;
//    mTypesDistribution[DComponent::Type::Operator] = 1.0f;
//    mTypesDistribution[DComponent::Type::Fraction] = 1.0f;
//    mTypesDistribution[DComponent::Type::Root] = 1.0f;
//    mTypesDistribution[DComponent::Type::SquareRoot] = 1.0f;
//    mTypesDistribution[DComponent::Type::Fences] = 1.0f;
//    mTypesDistribution[DComponent::Type::Sub] = 1.0f;
//    mTypesDistribution[DComponent::Type::Sup] = 1.0f;
//    mTypesDistribution[DComponent::Type::Over] = 1.0f;
//    mTypesDistribution[DComponent::Type::Under] = 1.0f;
//    mTypesDistribution[DComponent::Type::SubSup] = 1.0f;
//    mTypesDistribution[DComponent::Type::OverUnder] = 1.0f;
//    normalize(mTypesDistribution);

//    mMultiplicityDistribution = {1.0f, 1.0f, 1.0f, 1.0f};
//    normalize(mMultiplicityDistribution);

}

//DComponent* RandomComponentGenerator::next(int depth)
//{
//    DComponent::Type t = nextType();
//    switch (t) {
//    case DComponent::Type::Symbol:
//        return genSymbol();
//    case DComponent::Type::Operator:
//        return genOperator();
//    case DComponent::Type::Table:
//        return new DTableNode();
//    default:
//        return genStaticNode(t, depth + 1);
//    }

//}

//DNode *RandomComponentGenerator::getRandomChild(const DComponent *component)
//{
//    assert(component != nullptr);

//    std::vector<DNode*> nodes;
//    switch (component->getType()) {
//    case DComponent::Type::Operator:
//    case DComponent::Type::Symbol:
//        return nullptr;
//    case DComponent::Type::Row:{
//        assert(component->asNode() != nullptr);

//        auto lines = component->asNode()->getComponents();
//        for(auto line : lines)
//            for(auto component : line){
//                if(component->getType() == DComponent::Type::Row){
//                    DNode* n1 = component->asNode();
//                    if(n1 != nullptr)
//                        nodes.insert(nodes.end(), n1);
//                }

//                DNode* n2 = getRandomChild(component);
//                if(n2 != nullptr)
//                    nodes.insert(nodes.end(), n2);
//            }
//    }break;
//    case DComponent::Type::Table:{
//    }break;
//    default:
//        assert(component->asStaticNode() != nullptr);

//        auto components = component->asStaticNode()->getComponents();
//        for(auto component : components){
//            if(component->getType() == DComponent::Type::Row){
//                DNode* n1 = component->asNode();
//                if(n1 != nullptr)
//                    nodes.insert(nodes.end(), n1);
//            }
//            DNode* n2 = getRandomChild(component);
//            if(n2 != nullptr)
//                nodes.insert(nodes.end(), n2);
//        }
//        break;
//    }

//    if(nodes.size() == 0)
//        return nullptr;

//    std::uniform_int_distribution<int> d(0, static_cast<int>(nodes.size() - 1));
//    int i = d(mGenerator) ;
//    return nodes.at(static_cast<size_t>(i));
//}

//DComponent *RandomComponentGenerator::genSymbol()
//{
//    return new DSymbol(DComponent::Type::Symbol, "a");
//}

//DComponent *RandomComponentGenerator::genOperator()
//{
//    return new DSymbol(DComponent::Type::Operator, "+");
//}

//DComponent *RandomComponentGenerator::genStaticNode(DComponent::Type type, int depth)
//{
//    uint n_children = 0;
//    if(depth < mMaxDepth){
//        switch (type) {
//        case DComponent::Type::Fences:
//        case DComponent::Type::SquareRoot: n_children = 1; break;
//        case DComponent::Type::Fraction:
//        case DComponent::Type::Root:
//        case DComponent::Type::Sub:
//        case DComponent::Type::Sup:
//        case DComponent::Type::Over:
//        case DComponent::Type::Under: n_children = 2; break;
//        case DComponent::Type::SubSup:
//        case DComponent::Type::OverUnder: n_children = 3; break;
//        default: n_children = 0;
//        }
//    }

//    std::vector<DComponent*> children(n_children);
//    for(uint i = 0; i < n_children; i++)
//        children[i] = genNode(depth + 1);

//    return new DStaticNode(type, std::move(children));
//}

//DNode *RandomComponentGenerator::genNode(int depth)
//{
//    const uint n = depth < mMaxDepth ? nextMultiplicity() : 0;
//    std::vector<DComponent*> components(n);
//    for(uint i = 0; i < n; i++)
//        components[i] = next(depth + 1);
//    return new DNode({components});
//}


//DComponent::Type RandomComponentGenerator::nextType()
//{
//    std::uniform_real_distribution<float> dist6(0, 1.0f);
//    float r = dist6(mGenerator);
//    float t;
//    auto entry = mTypesDistribution.begin();
//    while(r > (t = (*entry).second)){
//        r -= t;
//        entry++;
//    }
//    return (*entry).first;
//}

//uint RandomComponentGenerator::nextMultiplicity()
//{
//   std::uniform_real_distribution<float> dist6(0, 1.0f);
//   float r = dist6(mGenerator);
//   size_t i = 0;
//   float t;
//   while(r > (t = mMultiplicityDistribution.at(i))){
//       r -= t;
//       i++;
//   }
//   return static_cast<uint>(i);
//}


