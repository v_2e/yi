#ifndef POOLTEST_H
#define POOLTEST_H

#include <string>

#include <unordered_map>


struct Entity{
    int a;
};

typedef std::unordered_map<std::string, Entity> font_size_map_t;
typedef std::unordered_map<std::string, font_size_map_t> glyphs_map_t;
typedef std::unordered_map<std::string, Entity> direct_map_t;


struct EntityRequest{
    std::string symbol;
    std::string font_name;
    int font_size;
};

class PoolTest
{

public:
    PoolTest();
    Entity getEntity(const EntityRequest &request);

private:
    void init();

    // approach 1:
    glyphs_map_t entities_map;

    // approach 2:
    direct_map_t direct_entities_map;

    void test();


};

#endif // POOLTEST_H
