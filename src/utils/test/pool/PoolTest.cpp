#include "PoolTest.h"

#include <iostream>
#include <fstream>
#include <vector>

#include <QElapsedTimer>
#include <QDebug>

PoolTest::PoolTest()
{
    init();
    test();
}

void PoolTest::init()
{

    QElapsedTimer timer;
    timer.start();

    // prepare default alphabet
    std::string alphabet = "abcdefghijklmnopqrstuwxyz"
                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                           "0123456789"
                           "!@#$%^&*()_+}{][?/.>,<";
    std::vector<char> glyphs(alphabet.begin(), alphabet.end());

    // prepare font database
    std::ifstream stream("fontnames.txt", std::ios::in);
    std::vector<std::string> fontnames;
    if(stream.is_open()){
        std::string line;
        while(getline(stream, line)){
            fontnames.push_back(line);
        }
    }else{
        qDebug() << "Can't open file";
    }

    // prepare font sizes:
    std::vector<int> sizes = {8, 10, 12, 14, 16, 18, 20, 22, 24};





    // for approach 1:
    int i = 1;
    for(auto glyph : glyphs){
        font_size_map_t m1;
        for(auto fontname : fontnames){
            for(auto fontsize: sizes){
                std::string id = fontname + " " + std::to_string(fontsize);
                Entity value = {i++};
                m1[id] = value;
            }
        }
        entities_map[std::string(1, glyph)] = m1;
    }
    qDebug() << "Total entries in map: " << i;


    // for approach 2:
    i = 0;
    for(auto glyph : glyphs){
        for(auto fontname : fontnames){
            for(auto fontsize: sizes){
                std::string id = std::string(1, glyph) + " " + fontname + " " + std::to_string(fontsize);
                Entity value = {i++};
                direct_entities_map[id] = value;
            }
        }
    }


    qDebug() << "Pool initialized in " << timer.elapsed() << " ms.";

}

Entity PoolTest::getEntity(const EntityRequest &request)
{

    // approach 1:
    try {

        font_size_map_t& m1 = entities_map[request.symbol];

        return m1[request.font_name + " " + std::to_string(request.font_size)];
//        return entities_map
//                [request.symbol]
//                [request.font_name + " " + std::to_string(request.font_size)];
    } catch (std::out_of_range e) {
        qDebug() << "Not found!";
        return Entity {0};
    }


    // approach 2:
//    try {
//        std::string id = request.symbol + " " +
//                         request.font_name + " " +
//                         std::to_string(request.font_size);
//        return direct_entities_map.at(id);
//    } catch (std::out_of_range e) {
//        qDebug() << "Not found!";
//        return Entity {0};
//    }
}

std::vector<EntityRequest> generate_requests(){
    std::vector<EntityRequest> requests;
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"z", "Aldus", 12});
    requests.push_back({"Z", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    requests.push_back({"a", "STIX", 12});
    requests.push_back({"X", "STIX", 12});
    requests.push_back({"{", "Cochin", 12});
    requests.push_back({"E", "Cochin", 12});
    requests.push_back({"/", "Aldus", 12});
    requests.push_back({"e", "Aldus", 12});
    return requests;
}

void PoolTest::test()
{

    std::vector<EntityRequest> requests = generate_requests();
    QElapsedTimer timer;
    int n = 10;
    long t = 0;
    for(int i = 0; i < n; i++){
        timer.start();
        for(auto r : requests){
            Entity e = getEntity(r);
//            qDebug() << e.a;
        }
        t += timer.elapsed();
    }
//    qDebug() << requests.size() << " requests processed in " << timer.elapsed() << " ms.";
    qDebug() << "Requests processed in " << t/n << " ms.";
}



