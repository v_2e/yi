#ifndef POOL_H
#define POOL_H

#include <unordered_map>

template <typename K, typename V>
class Pool
{

public:
    class Handle{
    public:
        V* get() { return reference; }


    private:
        V* reference;
        unsigned int useCounter;
        friend class Pool;
    };

public:
    Pool();
    bool contains(const K& key) const;
    Handle* get(const K& key);
    Handle* put(const K& key, V* value);
    void recycle(Handle *handle);
    void setUnusedLimit(unsigned int limit);
    void cleanup();

private:
    std::unordered_map<K, Handle*> mContainer;

private:
    int mUnusedCounter = 0;
    int mCleanupTriggerLimit = 10;

};

template<typename K, typename V>
Pool<K,V>::Pool()
{

}

template<typename K, typename V>
bool Pool<K,V>::contains(const K &key) const
{
    return mContainer.find(key) == mContainer.end();
}

template<typename K, typename V>
typename Pool<K,V>::Handle* Pool<K,V>::get(const K &key)
{
    try {
        Handle* handle = mContainer.at(key);
        handle->useCounter++;
        return handle;
    } catch (std::out_of_range &e) {
        return nullptr;
    }
}

template<typename K, typename V>
typename Pool<K,V>::Handle* Pool<K,V>::put(const K &key, V *value)
{
    Handle* handle = new Handle;
    handle->reference = value;
    handle->useCounter = 0;
    mContainer[key] = handle;
    return handle;
}

template<typename K, typename V>
void Pool<K,V>::recycle(Pool::Handle *handle)
{
    if(--handle->useCounter == 0)
        if(++mUnusedCounter >= mCleanupTriggerLimit)
            cleanup();
}

template<typename K, typename V>
void Pool<K,V>::setUnusedLimit(unsigned int limit)
{
    mCleanupTriggerLimit = limit;
    if(mUnusedCounter >= mCleanupTriggerLimit)
        cleanup();
}

template<typename K, typename V>
void Pool<K,V>::cleanup()
{
    auto i = mContainer.begin();
    while(i != mContainer.end()){
        if(mContainer.at(i).useCounter > 0){
            i++;
        }else{
            mContainer.erase(i);
        }
    }
    mUnusedCounter = 0;
}


#endif // POOL_H
